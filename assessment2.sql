USE assessment;


CREATE TABLE department
(
   dept_num    int(10),
   dept_name   varchar(30),
   location    varchar(33)
);

CREATE TABLE employee
(
   emp_num      int(4),
   emp_name     varchar(15),
   job          varchar(15),
   manager      char(4),
   hire_date    DATE,
   salary       int(6),
   commission   int(6),
   dept_num     int(2)
);

ALTER TABLE department
   MODIFY dept_num int(10) NOT NULL;

ALTER TABLE department
   MODIFY dept_name varchar(30) UNIQUE;

ALTER TABLE department ADD PRIMARY KEY (dept_num);

ALTER TABLE department
   MODIFY dept_name varchar(15) UNIQUE;

ALTER TABLE employee ADD PRIMARY KEY(emp_num);

ALTER TABLE employee
   MODIFY emp_name varchar(15) NOT NULL;

ALTER TABLE employee ADD FOREIGN KEY (dept_num) REFERENCES department(dept_num);

ALTER TABLE employee
   MODIFY salary DECIMAL(6, 2);

ALTER TABLE employee
   ADD COLUMN DOB DATETIME;

ALTER TABLE employee
   DROP COLUMN DOB;

INSERT INTO department(dept_num, dept_name, location)
     VALUES (10, 'Accounting', 'ECIL');

INSERT INTO department(dept_num, dept_name, location)
     VALUES (20, 'Constructing', 'Panjagutta');

INSERT INTO department(dept_num, dept_name, location)
     VALUES (30, 'Software', 'Madhapur');

INSERT INTO department(dept_num, dept_name, location)
     VALUES (40, 'Education', 'Dilsukh Nagar');

INSERT INTO employee(emp_num,
                     emp_name,
                     job,
                     manager,
                     hire_date,
                     salary,
                     dept_num)
     VALUES (7369,
             'SMITH',
             'CLERK',
             '7902',
             '1980:12:17',
             2800,
             20);

INSERT INTO employee(emp_num,
                     emp_name,
                     job,
                     manager,
                     hire_date,
                     salary,
                     dept_num)
     VALUES (7566,
             'JONES',
             'MANAGER',
             '7839',
             '1981:04:02',
             3570,
             20);

INSERT INTO employee(emp_num,
                     emp_name,
                     job,
                     manager,
                     hire_date,
                     salary,
                     commission,
                     dept_num)
     VALUES (7564,
             'MARTIN',
             'SALESMAN',
             '7968',
             '1981:09:28',
             1250,
             1400,
             30);

INSERT INTO employee(emp_num,
                     emp_name,
                     job,
                     manager,
                     hire_date,
                     salary,
                     dept_num)
     VALUES (7782,
             'CLARK',
             'MANAGER',
             '7839',
             '1981:06:09',
             2940,
             10);

INSERT INTO employee(emp_num,
                     emp_name,
                     job,
                     manager,
                     hire_date,
                     salary,
                     dept_num)
     VALUES (7788,
             'SCOTT',
             'ANALYST',
             '7566',
             '1982:12:09',
             3000,
             20);

INSERT INTO employee(emp_num,
                     emp_name,
                     job,
                     manager,
                     hire_date,
                     salary,
                     dept_num)
     VALUES (7839,
             'KING',
             'PRESIDENT',
             '7566',
             '1981:11:17',
             5000,
             10);

INSERT INTO employee(emp_num,
                     emp_name,
                     job,
                     manager,
                     hire_date,
                     salary,
                     commission,
                     dept_num)
     VALUES (7844,
             'TURNER',
             'SALESMAN',
             '7698',
             '1981:09:08',
             1500,
             0,
             20);

INSERT INTO employee(emp_num,
                     emp_name,
                     job,
                     manager,
                     hire_date,
                     salary,
                     dept_num)
     VALUES (7876,
             'ADAMS',
             'CLERK',
             '7788',
             '1983:01:12',
             3100,
             20);

INSERT INTO employee(emp_num,
                     emp_name,
                     job,
                     manager,
                     hire_date,
                     salary,
                     dept_num)
     VALUES (7900,
             'JAMES',
             'CLERK',
             '7698',
             '1981:12:03',
             2950,
             30);

INSERT INTO employee(emp_num,
                     emp_name,
                     job,
                     manager,
                     hire_date,
                     salary,
                     dept_num)
     VALUES (7902,
             'FORD',
             'ANALYST',
             '7566',
             '1981:12:03',
             3000,
             20);

INSERT INTO employee(emp_num,
                     emp_name,
                     job,
                     manager,
                     hire_date,
                     salary,
                     dept_num)
     VALUES (7934,
             'MILLER',
             'CLERK',
             '7782',
             '1982:01:23',
             3300,
             10);

INSERT INTO employee(emp_num,
                     emp_name,
                     job,
                     manager,
                     hire_date,
                     salary,
                     dept_num)
     VALUES (7521,
             'WARD',
             'SALESMAN',
             '7698',
             '1981:02:22',
             500,
             30);

INSERT INTO employee(emp_num,
                     emp_name,
                     job,
                     manager,
                     hire_date,
                     salary,
                     dept_num)
     VALUES (7698,
             'BLAKE',
             'MANAGER',
             '7839',
             '1981:05:01',
             3420,
             30);

INSERT INTO employee(emp_num,
                     emp_name,
                     job,
                     manager,
                     hire_date,
                     salary,
                     commission)
     VALUES (7777,
             'MILL',
             'ANALYST',
             '7839',
             '1981:05:01',
             2000,
             200);


SELECT * FROM department;

SELECT dept_num, location FROM department;

SELECT * FROM employee;

SELECT emp_num,
       emp_name,
       hire_date,
       dept_num
  FROM employee;

DESC department;
DESC employee;


ALTER TABLE department
   MODIFY dept_num int(2);

ALTER TABLE department
   MODIFY dept_name varchar(15);

ALTER TABLE department
   MODIFY location varchar(15);

ALTER TABLE employee
   MODIFY job varchar(10);

ALTER TABLE employee
   MODIFY manager decimal(5);

/*    THIS IS SYNTAX
   ALTER TABLE tablename
   DROP column column_name;
*/

  SELECT emp_name, emp_num
    FROM employee
   WHERE salary > 2600
ORDER BY emp_name;

  SELECT *
    FROM employee
   WHERE job = 'manager' OR job = 'president'
ORDER BY dept_num;

SELECT emp_name, job, dept_num
  FROM employee
 WHERE emp_name LIKE 'c%' OR 'd%' OR 'e%' OR 'f%';

  SELECT emp_name
    FROM employee
ORDER BY salary ASC;

  SELECT emp_name
    FROM employee
ORDER BY salary DESC;

  SELECT *
    FROM employee
ORDER BY emp_name;

  SELECT emp_num,
         emp_name,
         dept_num,
         salary
    FROM employee
ORDER BY emp_name, dept_num, salary;

  SELECT dept_num
    FROM employee
GROUP BY dept_num
  HAVING count(*) > 3;

UPDATE employee
   SET dept_num = 10
 WHERE dept_num = 20;

SELECT emp_name
  FROM employee
 WHERE salary < (SELECT (salary * 30 / 100)
                   FROM employee
                  WHERE job = 'president');

UPDATE employee
   SET emp_name = 'Indian'
 WHERE job = 'president';

DELETE FROM employee
      WHERE emp_num = 7876;

ALTER TABLE employee
   RENAME udyogulu;

ALTER TABLE udyogulu
   RENAME employee;

SELECT * FROM employee;

UPDATE employee
   SET salary = salary + (salary * (30 / 100))
 WHERE job = 'manager';



ALTER TABLE employee
   ADD COLUMN age numeric;

ALTER TABLE employee
   MODIFY salary numeric(6, 2);
   
   
   SELECT * FROM employee WHERE job='salesman';
   
   SELECT * FROM employee WHERE year(hire_date)>'1982';
   
   SELECT emp_num,emp_name,job,hire_date,salary FROM employee where job='manager';
   
   SELECT * FROM employee where commission>salary;
   
   SELECT *,YEAR(CURDATE())-YEAR(hire_date) AS experience FROM employee where salary>200;
   
   SELECT * FROM employee WHERE (salary+(salary*0.2))>3000;
   
   SELECT * FROM employee WHERE YEAR(CURDATE())-YEAR(hire_date)>32;
   
   SELECT * FROM employee WHERE salary>2000;
   
   SELECT emp_num,emp_name,job,hire_date FROM employee where dept_num IN (SELECT dept_num FROM department where dept_name='accounting');
   
   SELECT emp_name,salary FROM employee WHERE job='CLERK';
   
   SELECT d.dept_name,GROUP_CONCAT(e.emp_name) from employee e,department d where e.dept_num=d.dept_num GROUP BY(dept_name);
   
   SELECT d.dept_name,GROUP_CONCAT(e.job) from employee e,department d where e.dept_num=d.dept_num GROUP BY(dept_name);
   
   SELECT * FROM EMPLOYEE
WHERE Job = 'MANAGER' OR Job= 'CLERK';

SELECT Emp_Name AS EMPNAME, Salary AS MONTHLYSALARY FROM EMPLOYEE
WHERE Salary NOT BETWEEN 1500 AND 2800;

SELECT Emp_Name AS ENAME, Salary AS SAL, 
Salary+(Salary*0.15) AS NEWSAL, Salary+(Salary*0.15) - Salary AS INCREMENT FROM EMPLOYEE;

SELECT Emp_Num, Emp_Name FROM EMPLOYEE
WHERE Commission <> 0;

SELECT Emp_Num, Emp_Name FROM EMPLOYEE
WHERE Commission = 0;

SELECT Dept_Num, Dept_Name FROM DEPARTMENT
WHERE Dept_Num >= 20;

SELECT Emp_Name FROM EMPLOYEE
WHERE (Dept_Num = 10 OR Dept_Num = 20) AND (Job = 'CLERK' OR Job = 'MANAGER' OR Job = 'SALESMAN');

SELECT Emp_Name, Salary AS MONTHLY,
Salary/22 AS DAILY, (Salary/22)/8 AS HOURLY FROM EMPLOYEE;

SELECT Emp_Name FROM EMPLOYEE
WHERE Emp_Name LIKE 'J%';

SELECT Emp_Name, Job FROM EMPLOYEE
WHERE Job LIKE '%R';

SELECT Emp_Name FROM EMPLOYEE
WHERE CHAR_LENGTH(Emp_Name) = 4;

SELECT Emp_Name FROM EMPLOYEE
WHERE Emp_Name LIKE '__N%';

SELECT  D.Dept_Name, GROUP_CONCAT(E.Emp_Name) FROM DEPARTMENT D, EMPLOYEE E
WHERE D.Dept_Name = 'Accounting';

SELECT * FROM EMPLOYEE
WHERE Emp_Name LIKE 'T%R';

SELECT * FROM EMPLOYEE
WHERE Job = 'CLERK' AND YEAR(CURDATE()) - YEAR(Hire_Date) >= 12;

SELECT Emp_Name FROM EMPLOYEE
WHERE Emp_Name NOT LIKE 'S%';

SELECT Emp_Name FROM EMPLOYEE
WHERE Hire_Date = '1981:05:01';

SELECT Emp_Num, Emp_Name, Hire_Date FROM EMPLOYEE
WHERE Hire_Date < '1983:01:01';
   
   
   #Assignment 4
   
     CREATE VIEW min5
   AS
        SELECT dept_num
          FROM employee
      GROUP BY (dept_num)
        HAVING count(*) > 5;

   
   CREATE VIEW EmpDept
    AS
      SELECT e.emp_num,
             e.emp_name,
             e.job,
             e.manager,
             e.hire_date,
             e.salary,
             e.commission,
             e.dept_num,
             e.age,
             d.location
        FROM employee e, department d
       WHERE e.dept_num = d.dept_num;



   
SELECT emp_num, salary + commission TotalSalary FROM employee;

SELECT MAX(salary) FROM employee;

SELECT MIN(salary) FROM employee;

SELECT AVG(salary) FROM employee;

  SELECT MAX(salary)
    FROM employee
GROUP BY (job)
  HAVING job = 'clerk';

SELECT SUM(salary + commission) salaryDrawn
  FROM employee
 WHERE job = 'analyst' AND dept_num = 20;

  SELECT dept_num, count(*) NumberOfEmployees
    FROM employee
GROUP BY (dept_num);

  SELECT job, count(*) NumberOfEmployees
    FROM employee
GROUP BY (job);

  SELECT dept_num, SUM(salary) TotalSalary
    FROM employee
GROUP BY (dept_num);

  SELECT dept_num, MAX(salary) TotalSalary
    FROM employee
GROUP BY (dept_num);

SELECT emp_name
  FROM employee
 WHERE     salary > (  SELECT MAX(salary)
                         FROM employee
                     GROUP BY (job)
                       HAVING job = 'clerk')
       AND job = 'salesman';

  SELECT AVG(salary), MAX(salary), MIN(salary)
    FROM employee
GROUP BY (job)
  HAVING job = 'CLERK' OR 'MANAGER';

SELECT emp_name
  FROM employee
 WHERE salary IN (  SELECT MAX(salary)
                      FROM employee
                  GROUP BY (dept_num));

SELECT job
  FROM employee
 WHERE salary + commission > (  SELECT MAX(salary)
                                  FROM employee
                              GROUP BY (job)
                                HAVING job = 'manager');

SELECT FLOOR(111.99), CEIL(111.99), ROUND(111.99);

SELECT UPPER(emp_name) FROM employee;

SELECT LENGTH(dept_name) FROM department;

SELECT CURDATE();

SELECT INTERVAL 3 MONTH + CURDATE();

SELECT WEEKDAY(LAST_DAY(CURDATE()));

SELECT DATE_FORMAT(CURDATE(), '%d %m %Y'), DATE_FORMAT(CURDATE(), '%d %m %y');

UPDATE employee
   SET commission = IFNULL(commission, 0);

SELECT DISTINCT (job) FROM employee;

SELECT LEFT(emp_name, 3) FROM employee;

SELECT DISTINCT E.Dept_Num
  FROM EMPLOYEE E LEFT JOIN DEPARTMENT D ON D.Dept_Num <> E.Dept_Num;


SELECT *
  FROM employee e INNER JOIN department d ON e.Dept_Num = d.Dept_Num;



SELECT *
  FROM employee e, department d
 WHERE d.Dept_Num = e.Dept_Num;




SELECT e.Emp_Name AS 'Employee', m.Emp_Name AS 'reports to'
    FROM employee e INNER JOIN employee m ON m.Emp_Num = e.Manager
ORDER BY m.Manager;


SELECT * FROM EMPLOYEE E
INNER JOIN DEPARTMENT D ON E.Dept_Num = D.Dept_Num;


SELECT DISTINCT * FROM EMPLOYEE E
LEFT OUTER JOIN DEPARTMENT D ON D.Dept_Num = E.Dept_Num;


SELECT DISTINCT * FROM EMPLOYEE E
RIGHT OUTER JOIN DEPARTMENT D ON E.Dept_Num = D.Dept_Num;


SELECT * FROM EMPLOYEE E
LEFT JOIN DEPARTMENT D ON E.Dept_Num = D.Dept_Num
UNION
SELECT * FROM EMPLOYEE E
RIGHT JOIN DEPARTMENT D ON E.Dept_Num = D.Dept_Num;


SELECT *
  FROM employee e JOIN department d
 WHERE d.dept_num = e.dept_num;

SELECT DISTINCT (e.emp_name)
  FROM employee e
       INNER JOIN employee f
          ON e.dept_num = f.dept_num AND e.emp_name <> f.emp_name;
		  
SELECT emp_name
  FROM employee e
 WHERE     job = 'manager'
       AND salary > (SELECT AVG(salary)
                       FROM employee m
                      WHERE job <> 'manager' AND e.emp_num = m.manager);


SELECT e.emp_name,
       e.hire_date,
       e.job,
       d.dept_name
  FROM employee e, department d
 WHERE e.dept_num = d.dept_num AND e.dept_num = 10;

SELECT e.emp_name,
       e.hire_date,
       e.job,
       d.dept_name
  FROM employee e, department d
 WHERE e.dept_num = d.dept_num AND e.dept_num = 20;

SELECT emp_name
  FROM employee
 WHERE dept_num NOT IN (SELECT dept_num FROM department);


