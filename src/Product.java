


public class Product {

	String code;
	String name;
	String description;
	double price;
	long available;
	public Product(String code,String name,String description,double price,long available)
	{
		this.code=code;
		this.name=name;
		this.description=description;
		this.price=price;
		this.available=available;
		
	}
	
	void placeOrder(long quantity)
	{
		if(quantity<available)
		{
			System.out.println("Order successful. You have ordered "+quantity+" "+this.name+"(s)");
			available-=quantity;
		}
		else 
			System.out.println("Failed. '"+this.available+"' products are in stock. Place the order accordingly");
	}
	
	boolean isAvailable()
	{
	if(this.available>0)
		return true;
	return false;
	}
	
	public static void main(String[] args)
	{
		
		Product pen=new Product("Reynolds1234","Ballpoint pen","This is used to write",10.00,25);
		Product pencil=new Product("Natraj5678","HB Pencil","This is also used to write",5.00,30);
		Product stapler=new Product("Kangaroo9012","Kangaroo Stapler","Used for stapling",100.25,15);
		
		pen.placeOrder(10);
		if(pen.isAvailable())
		{
			System.out.println("Pens are in stock.");
		}
		pen.placeOrder(20);
	}
}
