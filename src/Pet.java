
public class Pet {
String owner;
long licenseNumber;
boolean trained;
public Pet(String owner,long licenseNumber,boolean trained)
{
	this.owner=owner;
	this.licenseNumber=licenseNumber;
	this.trained=trained;
}

public boolean isTrained()
{
	return trained;
}

}
