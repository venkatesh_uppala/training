import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CalenderExample {
	public static void main(String[] args) {
		Calendar c = Calendar.getInstance();
		c.set(2014, 7, 3);
		System.out.println(c.getTime());
		System.out.println(c.toString());
		c.set(Calendar.YEAR, 2014);
		// Date d=new Date();
		Date date=null;
		try {
			date = new SimpleDateFormat("yyyy.MM.dd").parse("2014.08.05");
		}

		catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern("yyyy.MM.dd");
		c.setTime(date);
		System.out.println(sdf.format(c.getTime()));
		System.out.println(date.getTime());
		System.out.println(c.get(6));
	}
}
