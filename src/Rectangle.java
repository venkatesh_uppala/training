
public class Rectangle {

	int x,y;
	double width,height;
	public Rectangle(int x,int y,double height,double width)
	{
		this.x=x;
		this.y=y;
		this.height=height;
		this.width=width;
	}
	public Rectangle(double height,double width)
	{
		this(0,0,height,width);
	}
	public void display()
	{
		System.out.println("Rectangle with co ordinates "+this.x+" and "+this.y+" with width "+this.width+" and height "+","+this.height );
	}
	public static void main(String[] args)
	{
		Rectangle r=new Rectangle(509.23,786.2378);
		r.display();
	}
}
