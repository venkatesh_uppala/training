
public class ExceptionExample extends Exception {
	
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 6699502548328460404L;

	public ExceptionExample()
	{
		super();
		System.out.println("This is custom exception\n");
	}
	
	public static void main(String... args) throws ExceptionExample
	{
		int age=10;
		
		if(age<18)
		{
			throw new ExceptionExample();
		}
		
	}

}
