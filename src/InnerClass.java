
public class InnerClass {
	
	public InnerClass()
	{
		System.out.println("In Enclosing instance");
	}
	class InnerInnerClass
	{
		public InnerInnerClass()
		{
			System.out.println("In Inner instance");
		}
		public void puli()
		{
			System.out.println("PuLi is here");
		}
	}
	public static void main(String[] args)
	{
		InnerClass inC=new InnerClass();
		InnerInnerClass ininC=inC.new InnerInnerClass();
		ininC.puli();
	}

}


