
public class Cat extends Pet {

	String color;
	String description;
	public Cat(String owner, long licenseNumber, boolean trained,String color,String description) {
		super(owner, licenseNumber, trained);
		// TODO Auto-generated constructor stub
		this.color=color;
		this.description=description;
	}
	public String toString()
	{
		return this.owner+" Owns a Cat with license number "+this.licenseNumber+" and it is "+this.color+" in color. Adil <3's "+this.description; 
	}
	public static void main(String[] args)
	{
		Cat pilli=new Cat("Adil",20145,true,"Black","Big Ol' Fur Ball");
		System.out.println(pilli.toString());
		if(pilli.isTrained())
			System.out.println("And yes it is trained.");
	}

}
