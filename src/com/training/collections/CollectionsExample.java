package com.training.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class CollectionsExample {

	
	public static void main(String[] args) {
		
		Vector<Integer> v = new Vector<>();
		ArrayList<Integer> al = new ArrayList<>();
		for (int j = 0; j < 1000; j++)
			al.add(j + 1);
		Object[] o = al.toArray();
		System.out.println(o[100]);
		System.out.println(o.length);
		Integer[] dup = new Integer[1200];
		Integer[] dup1 = al.toArray(dup);
		ArrayList<Integer> all = new ArrayList<>(1200);
		System.out.println(dup1.length);
		System.out.println(dup1[100]);
		all = al;
		System.out.println(all.size());
		all.add(new Integer(50));
		System.out.println(all.size());

		Iterator<Integer> i = al.iterator();
		LinkedList<Double> ll = new LinkedList<>();
		checkTimings("ArrayList", new ArrayList<Integer>());
		checkTimings("LinkedList", new LinkedList<Integer>());
	}

	public static void checkTimings(String type, List<Integer> list) {
		long start, end;
		System.out.println(start = System.currentTimeMillis());
		for (int i = 0; i < 100000; i++) {
			list.add(i);
		}
		System.out.println(end = System.currentTimeMillis());
		System.out.println(type + " took " + (end - start) + " milliseconds");

	}

}
