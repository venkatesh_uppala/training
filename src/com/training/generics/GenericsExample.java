package com.training.generics;
interface Swimmable
{
	void swim();
}
interface Flyable
{
	void fly();
}

interface Glidable extends Flyable
{
	void glide();
}
public class GenericsExample {
public static void main(String[] args)
{
	Parrot p=new Parrot();
	Bird b=p;
	b.makeSound();
	Parrot p1=(Parrot)b;
	p1.makeSound();
	
	//Parrot p2=(Parrot)new Bird(); ClassCastException ****RUntime Exception
	//Parrot p3=new Duck();     Cannot Cast ****Compiletime Exception
	//Parrot p4=(Parrot) new Duck();  Cannot Cast ****Compiletime Exception
	Bird bp=new Parrot();
	Flyable fbird=(Flyable)bp;
	Flyable fparrot=new Parrot();
	fbird.fly();
	Flyable flyparrot=new Parrot();
	Bird b2=(Bird)flyparrot;
	
	Glidable g=new Eagle();
	g.fly();
	g.glide();
	
	Flyable f=g;
	f.fly();
	
	
	Flyable fduck=new Duck();
	fduck.fly();
	Swimmable sduck=(Swimmable)fduck;
	sduck.swim();
	
	int[] arr={1,2,3};
	Cloneable c=arr;
	int[] arr1=(int[])c;
	
	
	Bird bArray[]=new Parrot[10];
	for(int i=0;i<10;i++)
	{
		bArray[i]=new Parrot();
	}
	
Parrot pArray[]=(Parrot[])bArray;	

pArray[1].fly();
	
	
Parrot p5=new Parrot("Yo");
Parrot p6=p5;
System.out.println("\n\n\n"+(p5==p6));
p5.string="Yoo";
System.out.println(p6.getName()==p5.getName());
	
}
}

class Bird
{
	public void makeSound()
	{
		System.out.println("Birrrrrddddd chirp");
	}
	
	
}

class Parrot extends Bird implements Flyable
{
	
String string;
Parrot()
{
	
}
Parrot(String s)
{
	this.string=s;
}


public String getName()
{
	return string;
}

	public void makeSound()
	{
		System.out.println("Parrrrrottttt burp");
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("I am flying......Iago");		
	}
	
	
	
}
class Duck extends Bird implements Flyable,Swimmable
{
	String name;
	Duck()
	{
		
	}
	Duck(String s)
	{
		name=s;
	}
	public void makeSound()
	{
		System.out.println("Duccckkkk quack");
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("Open season on ducks :D");
	}

	@Override
	public void swim() {
		// TODO Auto-generated method stub
		System.out.println("Swimming.....");
	}
}


class Eagle extends Bird implements Glidable
{

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("Eagle Flying");
	}

	@Override
	public void glide() {
		// TODO Auto-generated method stub
		System.out.println("Eagle Gliding");
	}
	
}


