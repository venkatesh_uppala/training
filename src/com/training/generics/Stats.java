package com.training.generics;

public class Stats<T extends Number> {
	T[] nums;
	int length;

	Stats(T[] array)
	{
		nums=array;
	}
	
	
	boolean sameAverage(Stats<? extends Number> o)
	{
		return(this.getAvg()==o.getAvg());
			
	}
	double getAvg()
	{
		double avg=0.0;
		for(T num:nums)
		{
			avg+=num.doubleValue();
		}
		return avg/nums.length;
	}
}
