package com.training.generics;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetTest {
	
static void isSuccessful(boolean status)
	{
		if(status)
		{
			System.out.println("Successful");
		}
		else
			
			System.out.println("Failed");
	}
	public static void main(String[] args)
	{
	Set<Bird> hs=new HashSet<>();
	Bird b=new Bird();
	hs.add(b);
	isSuccessful(hs.add(b));

//isSuccessful(hs.add(null));//true
//isSuccessful(hs.add(null));//false
for(Bird bb:hs)
{
	bb.makeSound();
}

Iterator<Bird> it=hs.iterator();

while(it.hasNext())
{
	it.next().makeSound();
}
if(hs.contains(b))
	System.out.println("Bird found!");

}
}
