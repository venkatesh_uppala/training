package com.training.generics;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;



public class ListTest {

	
	public static void main(String[] args)
	{
		List<Bird> birds=new ArrayList<Bird>();
		for(int i=0;i<3;i++)
		{
			birds.add(new Bird());
			birds.add(new Parrot());
			birds.add(new Duck());
			birds.add(new Eagle());
		}
		
		List<Parrot> parrots=new ArrayList<>(1);
		for(int i=0;i<3;i++)
		{
			System.out.println(""+parrots.size());
			parrots.add(new Parrot());
			
		}
		List<Bird> birds1=new ArrayList<>(birds);
		
		birds.addAll(parrots);
		
//		birds.add(new Cat());
		
		//birds.add(null);
		//birds.add(null);
		//birds.add(null);
		for(Bird b:birds)
		{
			b.makeSound();
		}
		
		System.out.println("\n\n");
		birds.get(7).makeSound();
		birds.set(9, new Bird());
		
		
		System.out.println(birds.size());
		
		Set<Bird> s=new HashSet<>(birds);
		
		if(s.containsAll(birds1))
		{
			System.out.println("Yes all birds are here.");
		}
		
		if(birds.containsAll(birds1))
		{
			System.out.println("Yes the birds are stinking this place too.");
		}
	}
}
