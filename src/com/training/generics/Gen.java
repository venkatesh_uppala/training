package com.training.generics;

class Gen<T>
{
	T bird;
	Gen(T bird)
	{
		this.bird=bird;
	}
	
	
	T getBird()
	{
		return this.bird;
	}
	
	void showType()
	{
		System.out.println(this.bird.getClass().getName());
	}
	public static void main(String[] args)
	{
		Gen<Parrot> ng=new Gen<Parrot>(new Parrot("Parrot"));
		ng.showType();
		Gen<Duck> ng1=new Gen<Duck>(new Duck());
		ng1.showType();
		
		Parrot p=ng.getBird();
		Duck d=ng1.getBird();
		
		
		//Parrot p1=ng1.getBird();
		
	}
}
