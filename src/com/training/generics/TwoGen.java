package com.training.generics;

class TwoGen<T,T1>
{
	T bird;
	T1 bird1;
	TwoGen(T bird,T1 bird1)
	{
		this.bird=bird;
		this.bird1=bird1;
	}
	
	
	T getBird()
	{
		return this.bird;
	}
	
	T1 getBird1()
	{
		return this.bird1;
	}
	
	void showType()
	{
		System.out.println(this.bird.getClass().getName());
	}
	static TwoGen getTwoBirds(String parrot,String duck)
	{
		return new TwoGen<Parrot,Duck>(new Parrot(parrot),new Duck(duck));
	}
	}
