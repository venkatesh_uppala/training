package com.training.generics;

class NonGen
{
	Bird bird;
	NonGen(Bird bird)
	{
		this.bird=bird;
	}
	
	
	Bird getBird()
	{
		return this.bird;
	}
	
	void showType()
	{
		System.out.println(this.bird.getClass().getName());
	}
	public static void main(String[] args)
	{
		NonGen ng=new NonGen(new Parrot("Parrot"));
		ng.showType();
		NonGen ng1=new NonGen(new Duck());
		ng1.showType();
		
		Parrot p=(Parrot)ng.getBird();
		Duck d=(Duck)ng1.getBird();
		
		//Parrot p1=(Parrot)ng1.getBird();
		
	}
}