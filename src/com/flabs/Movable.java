package com.flabs;
public interface Movable {

	public abstract void move();
}
