package com.flabs.conversions;


interface Swimmable
{
	void swim();
}
interface Flyable
{
	void fly();
}

interface Glidable extends Flyable
{
	void glide();
}
public class Conversions {
	        
        
        
    
public static void main(String[] args)
{
	Parrot p=new Parrot();
	Bird b=p;
	b.makeSound();
	Parrot p1=(Parrot)b;
	p1.makeSound();
    	
	//Parrot p2=(Parrot)new Bird(); ClassCastException ****RUntime Exception
	//Parrot p3=new Duck();     Cannot Cast ****Compiletime Exception
	//Parrot p4=(Parrot) new Duck();  Cannot Cast ****Compiletime Exception
	Bird bp=new Parrot();
	Flyable fbird=(Flyable)bp;
	Flyable fparrot=new Parrot();
	fbird.fly();
	Flyable flyparrot=new Parrot();
	Bird b2=(Bird)flyparrot;
	
	
	Glidable g=new Eagle();
	g.fly();
	g.glide();
	
	Flyable f=g;
	f.fly();
	
	
	Flyable fduck=new Duck();
	fduck.fly();
	Swimmable sduck=(Swimmable)fduck;
	sduck.swim();
	
	int[] arr={1,2,3};
	Cloneable c=arr;
	int[] arr1=(int[])c;
	
	
	Bird bArray[]=new Parrot[10];
	for(int i=0;i<10;i++)
	{
		bArray[i]=new Parrot();
	}
	
Parrot pArray[]=(Parrot[])bArray;	

pArray[1].fly();
	
	System.out.println(p.toString() +"\n"+p.hashCode());
	Parrot pppp=p;
	System.out.println(pppp.toString() +"\n"+pppp.hashCode());
	
	
}
}

class Bird
{
	public void makeSound()
	{
		System.out.println("Birrrrrddddd chirp");
	}
	
	
}

class Parrot extends Bird implements Flyable
{
	String name;
	
	public Parrot()
	{
		
	}
	public Parrot(String name)
	{
		this.name=name;
	}
	
	public boolean equals(Parrot obj) {
		if(obj==null)
			return false;
	return (obj.name.equals(this.name));
		
	}
	public void makeSound()
	{
		System.out.println("Parrrrrottttt burp");
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("I am flying......Iago");		
	}
	
	
	
	
	
}
class Duck extends Bird implements Flyable,Swimmable
{
	public void makeSound()
	{
		System.out.println("Duccckkkk quack");
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("Open season on ducks :D");
	}

	@Override
	public void swim() {
		// TODO Auto-generated method stub
		System.out.println("Swimming.....");
	}
}


class Eagle extends Bird implements Glidable
{

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("Eagle Flying");
	}

	@Override
	public void glide() {
		// TODO Auto-generated method stub
		System.out.println("Eagle Gliding");
	}
	
}