package com.flabs.bank;
import java.util.Random;
import com.flabs.people.Person;


public class Account {
protected double interestRate;
private long accountNumber;
private String accountHolder;
private double balance;
static Random r=new Random();
public Account()
{
	
}
public Account(Person p)
{
	
	this(6.0,r.nextInt(),p.getName(),r.nextDouble());
	
}
public Account(double interestRate,long accountNumber,String accountHolder,double balance)
{
	this.setInterestRate(interestRate);
	this.setAccountNumber(accountNumber);
	this.setAccountHolder(accountHolder);
	this.setBalance(balance);
}
public Account getAccount(Person p)
{
	return new Account(p);
}
public double getInterestRate()
{
	return interestRate;
}
public void setInterestRate()
{
	this.interestRate=5;
}
/**
 * @return the accountHolder
 */
public String getAccountHolder() {
	return accountHolder;
}
/**
 * @param accountHolder the accountHolder to set
 */
public void setAccountHolder(String accountHolder) {
	this.accountHolder = accountHolder;
}
/**
 * @return the accountNumber
 */
public long getAccountNumber() {
	return accountNumber;
}
/**
 * @param accountNumber the accountNumber to set
 */
public void setAccountNumber(long accountNumber) {
	this.accountNumber = accountNumber;
}
/**
 * @return the balance
 */
public double getBalance() {
	return balance;
}
/**
 * @param balance the balance to set
 */
public void setBalance(double balance) {
	this.balance = balance;
}
/**
 * @param interestRate the interestRate to set
 */
public void setInterestRate(double interestRate) {
	this.interestRate = interestRate;
}

}
