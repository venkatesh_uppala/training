package com.flabs.bank;
public class SavingsAccount extends Account {

	public SavingsAccount(double interestRate, long accountNumber,
			String accountHolder,double balance) {
		super(interestRate, accountNumber, accountHolder,balance);
		// TODO Auto-generated constructor stub
	
	}

	@Override
	public void setInterestRate()
	{
		interestRate=6;
		
	}
	
	//OVERLOADING setInterestRate()
	public void setInterestRate(double threshold)
	{
		if(getBalance()>threshold)
		{
			interestRate=10;
		}
	}
	public static void main(String[] args)
	{
		SavingsAccount sa=new SavingsAccount(4, 1023, "Adil", 5000.00);
		sa.setInterestRate();
		System.out.println(sa.getInterestRate());
		
	}
}
