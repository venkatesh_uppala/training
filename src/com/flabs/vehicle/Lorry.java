package com.flabs.vehicle;

public class Lorry {
	
	Engine engine;
	public Lorry()
	{
		engine=new Engine();
	}
	class Engine
	{
		int cc=10;
		
		
		public int getCC()
		{
			return cc;
		}
	}
	
	public static void main(String[] args)
	{
		Lorry lorry=new Lorry();
		System.out.println(lorry.engine.getCC());
	}
	

}
