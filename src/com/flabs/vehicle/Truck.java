package com.flabs.vehicle;

import java.util.Scanner;

public class Truck extends Vehicle {
	static Scanner sc;
	public Truck()
	{
		sc=new Scanner(System.in);

		
	}
	public Truck(String fuel,int numOfWheels,String engine,boolean isBio)
	{
		this.fuel=fuel;
		this.engine=engine;
		this.isBio=isBio;
		this.numOfWheels=numOfWheels;
	}

	@Override
	public void move() {
		// TODO Auto-generated method stub
		System.out.println("I am in "+getClass()+" class and I am moving on "+numOfWheels+" wheels");
	}

	@Override
	boolean isBio() {
		// TODO Auto-generated method stub
		return isBio;
	}

	public void getData()
	{
		System.out.println("Enter the type of fuel");
		fuel=sc.next();
		System.out.println("Enter number of wheels");
		numOfWheels=sc.nextInt();
		System.out.println("Enter Engine type");
		engine=sc.next();
		System.out.println("Enter if the vehicle is environmental friendly");
		isBio=sc.nextBoolean();
		
	}

}
