package com.flabs.vehicle;
import java.util.Scanner;
public class Car extends Vehicle {
	
	static Scanner sc;
	public Car()
	{
		sc=new Scanner(System.in);

		this.numOfWheels=4;
	}
	public Car(String fuel,String engine,boolean isBio)
	{
		this.fuel=fuel;
		this.engine=engine;
		this.isBio=isBio;
		this.numOfWheels=4;
	}
	
	

	@Override
	public void move() {
		// TODO Auto-generated method stub
		if(numOfWheels!=0)
		System.out.println("I am in "+getClass()+" class and I am moving on "+numOfWheels+" wheels");
		else 
		{
			System.out.println("Please enter date before you call this method");
			getData();
		}
	}

	@Override
	boolean isBio() {
		// TODO Auto-generated method stub
		if(numOfWheels!=0)
		return isBio;
		else 
		{
			System.out.println("Please enter date before you call this method");
			getData();
			return isBio;
		}
	}

	public void getData()
	{
		System.out.println("Enter the type of fuel");
		fuel=sc.next();
		System.out.println("Enter Engine type");
		engine=sc.next();
		System.out.println("Enter if the vehicle is environmental friendly");
		isBio=sc.nextBoolean();
	}
}
