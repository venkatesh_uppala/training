package com.flabs.people;
public class Student extends Person {

	String school;
	char grade;
	public Student(String name, String address, boolean awake,String school,char grade) {
		super(name, address, awake);
		// TODO Auto-generated constructor stub
		System.out.println("*************Student's constructor*************");
		this.school=school;
		this.grade=grade;
	}
	public void study()
	{
		System.out.println("*************Study method in Student class*************");
		if(isAwake())
		{
		System.out.println("Studying");	
		}
		else
			System.out.println("Not studying");
		
	}

}
