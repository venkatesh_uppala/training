package com.flabs.people;
public class Person {
String name;
private String address;
private boolean awake;
public Person(String name,String address,boolean awake)
{
	this.setName(name);
	this.setAddress(address);
	this.awake=awake;
	System.out.println("*************Person's constructor*************");
	
}
public boolean isAwake()
{
	System.out.println("*************isAwake Method in Person class*************");
	return this.awake;
}
/**
 * @return the name
 */
public String getName() {
	return name;
}
/**
 * @param name the name to set
 */
public void setName(String name) {
	this.name = name;
}
/**
 * @return the address
 */
public String getAddress() {
	return address;
}
/**
 * @param address the address to set
 */
public void setAddress(String address) {
	this.address = address;
}
}
