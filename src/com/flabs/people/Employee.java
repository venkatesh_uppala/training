package com.flabs.people;
public class Employee extends Person {

	String company;
	double salary;
	public Employee(String name,String address,boolean awake,String company,double salary)
	{
		super(name,address,awake);
		System.out.println("*************Employee's constructor*************");
		this.company=company;
		this.salary=salary;
		
	}
	public void increment(double amount)
	{
		System.out.println("*************Increment method of Employee class*************");
		System.out.print("Salary is increased by "+amount+" rupees and total salary is ");
		this.salary+=amount;
		System.out.println(this.salary);
		
	}
	
}
