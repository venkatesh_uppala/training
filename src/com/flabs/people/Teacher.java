package com.flabs.people;
public class Teacher extends Employee {

	String title;
	String specialization;
	String description;
	public Teacher(String name, String address, boolean awake, String company,
			double salary,String title,String description,String specialization) {
		super(name, address, awake, company, salary);
		// TODO Auto-generated constructor stub
		System.out.println("*************Teacher's constructor*************");
		this.title=title;
		this.description=description;
		this.specialization=specialization;
	}
	
	public boolean isSpecialized()
	{
		System.out.println("*************isSpecialized method in Teacher class*************");
		if(specialization!=null)
			return true;
		return false;
	}

	public void teaches()
	{
		System.out.println("*************Teaches method in Teacher class***************");
		System.out.print("I am a teacher and I teach ");
		switch(specialization)
		{
		case "IT":System.out.println("Computer Programming.");
					break;
		case "CSE":System.out.println("Computer Programming as well");
					break;
		case "MECH":System.out.println("Physics and Mechanics");
					break;
		case "ECE": System.out.println("Electronics and Communication");
					break;
		case "EEE":System.out.println("Electrical and Electronics");
					break;
		case "CIVIL":System.out.println("Structures");
					break;
		case "CHEMICAL":System.out.println("Chemistry. I am Walter White xD :D");
					break;
		default:System.out.println("Nothing. Just ordinary teacher who doesn't do anything but has all of our marks :D.");
					break;
		}
	}

}
