package com.flabs.people;
public class Engineer extends Employee {

	
	String title;
	String specialization;
	String description;
	public Engineer(String name, String address, boolean awake,
			String company, double salary,String title,String description,String specialization) {
		super(name, address, awake, company, salary);
		// TODO Auto-generated constructor stub
		System.out.println("*************Engineer's constructor*************");
	this.title=title;
	this.description=description;
	this.specialization=specialization;
	}

	public boolean isSpecialized()
	{
		System.out.println("*************isSpecialized method of Engineer class*************");
		if(specialization!=null)
			return true;
		return false;
	}
	
	public void design()
	{
		System.out.println("*************Design method of Engineer class*************");
		System.out.print("I am an Engineer and I design ");
		switch(specialization)
		{
		case "IT":System.out.println("Softwares");
					break;
		case "CSE":System.out.println("Softwares as well");
					break;
		case "MECH":System.out.println("Cars and other automotives");
					break;
		case "ECE": System.out.println("Electronic goods and communication stuff");
					break;
		case "EEE":System.out.println("Electrical and Electronic goods");
					break;
		case "CIVIL":System.out.println("Buildings and much more");
					break;
		case "CHEMICAL":System.out.println("Methamphetamine. Breaking Bad 8| xD");
					break;
		default:System.out.println("Nothing. Just ordinary engineer who doesn't exist.");
					break;
		}
	}
}
