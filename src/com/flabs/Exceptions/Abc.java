package com.flabs.Exceptions;

//import java.util.logging.Handler;
//import java.util.logging.LogRecord;
import java.util.logging.Logger;

//import org.apache.logging.log4j.Level;
//import org.apache.logging.log4j.core.*;

public class Abc {
	
	//private static Logger ul=Logger.getLogger("Abc");
	Logger l=Logger.getLogger("Abc");
	void abc()
	{
		   
       l.setLevel(java.util.logging.Level.SEVERE);
       l.info("MyExpception2");
       throw new MyException2();    
		
	}
	
	void def() throws MyException1
	{
		
		throw new MyException1();
	}
	
	class MyException2 extends RuntimeException
	{
		
	}
	class MyException1 extends Exception
	{
		
	}
	
	
	public static void main(String[] args)
	{
		Abc ab=new Abc();
		ab.abc();
		try
		{
		ab.def();
		}
		catch(Exception e)
		{
			
		}
		
		
		
	}

}
