
package com.flabs.animal;
import com.flabs.Movable;
public abstract class Animal implements Movable {
	String color;
	String makesSound;
	
	public abstract void eat();
	public abstract String makesSound();
}
