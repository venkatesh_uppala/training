package com.flabs.animal;

public class Dog extends Pet {
	public Dog(boolean trained,boolean wild,String makesSound,String color)
	{
		this.trained=trained;
		this.wild=wild;
		this.makesSound=makesSound;
		this.color=color;
	}
	@Override
	public boolean isTrained() {
		// TODO Auto-generated method stub
		return trained;
	}



	@Override
	public void move() {
		// TODO Auto-generated method stub
		System.out.println("I am in Dog's move method and I am moving");
		
	}

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		System.out.println("I am in Dog's ea+ method and dog is eating a hotdog");
	}
	@Override
	public String makesSound() {
		// TODO Auto-generated method stub
		return ("I "+makesSound+" and I am a dog");		
	}
	@Override
	public boolean isWild() {
		// TODO Auto-generated method stub
		return wild;
	}

}
