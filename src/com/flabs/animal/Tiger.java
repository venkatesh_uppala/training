package com.flabs.animal;

public class Tiger extends Wild {

	public Tiger(boolean tamed,boolean wild,String makesSound,String color)
	{
		this.tamed=tamed;
		this.wild=wild;
		this.makesSound=makesSound;
		this.color=color;
	}
	@Override
	public boolean isTamed() {
		// TODO Auto-generated method stub
		return tamed;
	}
    @Override
	public boolean isWild() {
		// TODO Auto-generated method stub
		return wild;
	}

	@Override
	public void move() {
		// TODO Auto-generated method stub
		System.out.println("I am in Tiger's move method and I like move it move it!!! :D");
	}

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		System.out.println("I am in Tiger's eat method and tiger is having a steak dinner.");
	}
	@Override
	public String makesSound() {
		// TODO Auto-generated method stub
		return ("I "+makesSound+" and I am a Tiger");
	}

}
