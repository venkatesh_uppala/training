package com.flabs.animal.test;

import com.flabs.Movable;
import com.flabs.animal.Animal;
import com.flabs.animal.Cat;
import com.flabs.animal.Dog;
import com.flabs.animal.Lion;
import com.flabs.animal.Pet;
import com.flabs.animal.Tiger;

public class TestAnimal {

	public static void main(String[] args)
	{
		Tiger t=new Tiger(false,true,"Roar","Yellow and black Stripes");
		Lion l=new Lion(true,false,"Roar","Light Yellow");
		Cat c=new Cat(true,true,"Meow","Inherited");
		Dog d=new Dog(true,false,"Bark","Inherited");
		
		Animal a=new Dog(true,false,"Bark","Inherited");
		Movable m=new Cat(true,true,"Meow","Inherited");
		System.out.println(a.makesSound());
		a.eat();
		a.move();
		
		System.out.println("\n\n");
		
		m.move();
		
		Dog dog=(Dog)a;
		
		System.out.println("\n\n");
		
		System.out.println(d.makesSound());
		d.eat();
		d.move();
		d.isTrained();
		d.isWild();
		
		System.out.println(a.toString()+"\t\t"+dog.toString());
		
		
		Dog dd=(Dog)a;
		Cat cc=(Cat)m;
		
		
		/*
		System.out.println(a.toString());
		
		System.out.println("\n\n");
		
		System.out.println(m.toString());
		
		System.out.println("\n\n");
		
		
		
		
		
		
		
		
		System.out.println("This tiger is "+(t.isTamed()?"tamed":"Not tamed"));
		t.eat();
		t.move();
		System.out.println(t.makesSound());
		System.out.println("\n\n");
		System.out.println("This Lion is "+(l.isTamed()?"tamed":"Not tamed"));
		l.eat();
		l.move();
		System.out.println(l.makesSound());
		*/
		
	}
}
