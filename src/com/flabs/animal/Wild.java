package com.flabs.animal;

public abstract class Wild extends Animal {
	
	boolean tamed;
	public abstract boolean isTamed();
	boolean wild;
	public abstract boolean isWild();

}
