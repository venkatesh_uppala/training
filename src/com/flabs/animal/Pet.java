package com.flabs.animal;

public abstract class Pet extends Animal {

	boolean trained;
	public abstract boolean isTrained();
	boolean wild;
	public abstract boolean isWild();
}
