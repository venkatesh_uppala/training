
public class Stock {
	String stockSymbol;
	String companyName;
	double price;
	boolean listed;
	public Stock(String stockSymbol,String companyName,double price,boolean listed)
	{
	this.stockSymbol=stockSymbol;
	this.companyName=companyName;
	this.price=price;
	this.listed=listed;
	}
	public void updatePrice(double value)
	{
		this.price=value;
	}
	public boolean isListed()
	{
		return this.listed;
	}
	public String toString()
	{
		
	return ""+this.stockSymbol+" --- "+this.companyName+" --- "+this.price+" --- "+(this.listed?"Listed":"Not listed");	
	}
	public static void main(String[] args)
	{
		Stock reliance=new Stock("RIL","Reliance",100.234,true);
		Stock TATA=new Stock("TATA","Tata Industries",89.7326,true);
		Stock venkiTech=new Stock("VenkiTech","Venki Industries",00.00,false);
		if(reliance.isListed())
			reliance.updatePrice(167.2345);
		System.out.println(reliance.toString());
	}

}
