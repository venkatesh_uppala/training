import java.util.Scanner;


public class ControlFlowException  {
	public static void main(String[] args)
	{
		Scanner sc=new Scanner(System.in);
		int control=0;
		while(control!=-1)
		{
			System.out.println("\nEnter the choice\1. Exception1\n 2.Exception2\n 3.Exception3");
			control=sc.nextInt();
			try
			{
			switch(control)
			{
			case 1:throw new MyException1();
					
			case 2:throw new MyException2();
					
			case 3:throw new MyException3();
					
			default:System.out.println("Wrong choice!!!!");
			}
			}
			catch(MyException3 me)
			{
				System.out.println("Catch of MyException-III");
			}
			catch(MyException2 me)
			{
				System.out.println("Catch of MyException-II");
			}
			catch(MyException1 me)
			{
				System.out.println("Catch of MyException-I");
			}
			
			
		}
		
	}

}


class MyException1 extends Exception
{
	public MyException1() {
		// TODO Auto-generated constructor stub
		super();
		System.out.println("MyException-I");
	}
}

class MyException2 extends MyException1
{
	
	public MyException2() {
		// TODO Auto-generated constructor stub
		super();
		System.out.println("MyException-II");
	}
}

class MyException3 extends MyException2
{
	public MyException3() {
		// TODO Auto-generated constructor stub
		super();
		System.out.println("MyException-III");
	}
	
}