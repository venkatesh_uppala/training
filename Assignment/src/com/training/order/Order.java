package com.training.order;

import java.util.ArrayList;
import java.util.List;

import com.training.customer.Customer;
import com.training.orderitem.OrderItem;
import com.training.product.Product;

public class Order {

	private Customer customer;

	public enum Status {
		ORDER_PLACED("Your order has been placed."), UNDER_PROCESSING(
				"Your order is under processing and will be approved after confirming financial credentials."), APPROVED(
				"Your order has been approved. It will be shipped shortly."), SHIPPED(
				"Your product has been shipped."), CANCELLED(
				"Sorry but your order has been cancelled.");
		private final String status;

		Status(String status) {
			this.status = status;
		}
	}

	private Status status;
	private final long orderNum;
	private String orderDate;
	private String shippingAddress;
	private List<OrderItem> orderedItems;

	public Order(Customer customer, long orderNum, String orderDate,
			String shippingAddress, Status status) {
		this.customer = customer;
		this.status = Status.ORDER_PLACED;
		this.orderNum = orderNum;
		this.orderDate = orderDate;
		this.shippingAddress = shippingAddress;
		this.status = status;
	}

	public static Order createOrder(Customer customer, long orderNum,
			String orderDate, String shippingAddress, Status status) {
		return new Order(customer, orderNum, orderDate, shippingAddress,
				status);
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getshippingAddress() {
		return shippingAddress;
	}

	public void setshippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public List<OrderItem> getOrderItems() {
		return orderedItems;
	}

	public void addOrderItem(OrderItem oi) {
		if (orderedItems == null)
			orderedItems = new ArrayList<>();
		orderedItems.add(oi);
	}

	public void print() {
		if (orderedItems != null) {
			System.out.println("\n" + customer.getName()
					+ " has placed order for \n");
			System.out.println("Date\t\tProduct\tQuantity    Price\tAddress");
			System.out.println("----\t\t-------\t--------    -----\t-------");
			for (OrderItem oi : orderedItems) {
				Product p = oi.getProduct();
				System.out.println(orderDate + "\t" + p.getName() + "\t"
						+ oi.getQuantity() + "\t" + p.getPrice() + "\t"
						+ shippingAddress);
			}
		} else
			System.out.println("There are no products in this order.");
	}

	public long getOrderNum() {
		return orderNum;
	}

}
