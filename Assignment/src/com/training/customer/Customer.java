package com.training.customer;
public class Customer {
private final long id;
private String name;
private String address;
private String phoneNum;
public String toString()
{
	return ""+this.name+"-----"+this.address+"-----"+this.phoneNum;
}

public Customer(long id,String name,String address,String phoneNum)
{
	this.id=id;
	this.name=name;
	this.address=address;
	this.phoneNum=phoneNum;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public String getphoneNum() {
	return phoneNum;
}

public void setphoneNum(String phoneNum) {
	this.phoneNum = phoneNum;
}

public long getId() {
	return id;
}

}
