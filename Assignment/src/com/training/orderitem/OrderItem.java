package com.training.orderitem;



import com.training.product.Product;

public class OrderItem {
	
	private Product product;
	private int quantity;
	private double purchasePrice;
	
	OrderItem(Product product,int quantity,double purchasePrice)
	{
		this.product=product;
		this.quantity=quantity;
		this.purchasePrice=purchasePrice;
	}

	public double getPurchasePrice() {
		return purchasePrice;
	}

	protected void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public int getQuantity() {
		return quantity;
	}

	public Product getProduct() {
		return product;
	}

	public static OrderItem placeOrderItem(Product product,int quantity,double purchasePrice)
	{
		return new OrderItem(product,quantity,purchasePrice);
	}
	
	

}
