package com.training.orderitem;

import com.training.product.TestProduct;

public class TestOrderItem {

	static OrderItem oi;
	static double purchase_price = 45599.99;

	public static OrderItem create() {
		return new OrderItem(TestProduct.create(), 1, 45599.99);
	}

	public static void test() {
		oi = create();
		assert (purchase_price == oi.getPurchasePrice());
	}

	public static void main(String[] args) {
		test();
	}

}
