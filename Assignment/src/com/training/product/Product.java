package com.training.product;
public class Product {
	
	private final long productCode;
	private String name;
	private String description;
	private double price;
	private long stock;
	
	
	public Product(long productCode,String name,String description,double price,long stock)
	{
	
		this.productCode=productCode;
		this.name=name;
		this.description=description;
		this.price=price;
		this.stock=stock;
	}
	
	
	public String toString()
	{
		return "Code : "+this.getproductCode()+"\nDescription : "+this.getDescription()+"\nPrice : "+this.getPrice()+"\nStock : "+this.getStock();
	}
	
	boolean isAvailable()
	{
		return (getStock()>0);
	}


	public long getproductCode() {
		return productCode;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}


	public long getStock() {
		return stock;
	}


	public void setStock(long stock) {
		this.stock = stock;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	

}
