package com.training.product;

public class TestProduct {
	static Product p;
	static String name = "PS4";

	public static Product create() {
		return new Product(12345, "PS4", "Gaming console", 45599.99, 100);
	}

	public static void test() {
		p = create();
		assert (name.equals(p.getName()));
	}

	public static void main(String[] args) {
		test();
	}
}
