import com.training.order.Order;
import com.training.order.TestOrder;
import com.training.orderitem.TestOrderItem;

public class Test {
	public static void main(String[] args) {
		Order order = TestOrder.create();
		order.addOrderItem(TestOrderItem.create());
		order.print();
	}
}
