package maps;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class MapExample {

	static Map<String, Integer> map = new HashMap<>();

	void findPhoneNum(int name) {
		if (map.containsValue(name)) {
			System.out.println(map.get(name));
		}
	}

	Map<Integer, String> populate(Map<Integer, String> map) {
		map.put(1, "Adil");
		map.put(3, "Cycle chain Adil");
		map.put(2, "Sher Dil");
		map.put(0, "DragvHi");
		map.put(9, "Sabjiya");
		map.put(24, "Venki");
		return map;

	}

	public static void main(String[] args) {

		map.put("Adil", 900000000);
		map.put("Sher dil", 900000001);
		map.put("PuLi", 784293291);
		map.put("Venki", 784293291);

		System.out.println(map);
		map.put("Venki", 90308483);
		System.out.println(map);

		System.out.println(map.containsKey("Venki"));

		System.out.println(map.containsValue(784293290));

		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			System.out.println(entry.getKey() + "  =  " + entry.getValue());

			MapExample me = new MapExample();
			Hashtable<Integer, String> mapp = new Hashtable<>();

			mapp = (Hashtable<Integer, String>) me.populate(mapp);

			Map<Integer, String> tree = new TreeMap<>();
			Map<Integer, String> link = new LinkedHashMap<>();
			tree = me.populate(tree);
			link = me.populate(link);
			System.out.println(mapp);
			System.out.println(tree);
			System.out.println(link);
			
			
		}
	}
}
