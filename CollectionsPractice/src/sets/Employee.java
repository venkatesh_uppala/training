package sets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



public class Employee implements Comparable<Employee>{
	double height;
	String name;
	Employee(double height,String name)
	{
		this.height=height;
		this.name=name;
	}
	public static void main(String[] args)
	{
		List<Employee> list=new ArrayList<>();
		Employee emp1=new Employee(50,"Ivan Draghavi");
		Employee emp2=new Employee(30,"Amelia Sabjiya");
		list.add(emp1);
		list.add(emp2);
		Collections.sort(list);
		for(Employee e:list)
		{
			System.out.println(e.name);
		}
		
	}

	@Override
	public int compareTo(Employee e) {
		// TODO Auto-generated method stub
		if(this.height==e.height)
		return 0;
		if(this.height>e.height)
			return 1;
		else 
			return -1;
	}

}
