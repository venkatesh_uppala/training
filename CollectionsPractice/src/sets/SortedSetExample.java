package sets;

import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;

public class SortedSetExample {

	static SortedSet<Integer> populate(SortedSet<Integer> set)
	{
		
		for(int i=1;i<20;i++)
		{
			try {
				set.add(Random.class.newInstance().nextInt(50));
			} catch (InstantiationException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return set;
	}
	public static void main(String[] args)
	{
		double start=System.currentTimeMillis();
	SortedSet<Integer> set=new TreeSet<>();
	set=populate(set);
	for(Integer i:set)
	{
		System.out.println(i);
	}
	System.out.println("\n\n"+(System.currentTimeMillis()-start));
	}

}
