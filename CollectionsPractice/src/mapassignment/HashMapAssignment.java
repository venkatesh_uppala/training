package mapassignment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class HashMapAssignment {

	private BufferedReader input = new BufferedReader(new InputStreamReader(
			System.in));
	private String word;

	public String getWord() {
		return word;
	}

	public HashMapAssignment(String word) {
		this.word = word;
	}

	private HashMap<String, Integer> map = new HashMap<>();

	void readWord() {
		System.out.println("Enter the words/characters");
		try {
			word = input.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void enterIntoMap(String inputWord) {
		String[] words;

		if (inputWord != null || inputWord != "") {

			words = inputWord.split(" ");
			System.out.println("No. of words : " + words.length);
			for (String nowWord : words) {

				if (map.containsKey(nowWord)) {
					int count = map.get(nowWord);
					count++;
					map.put(nowWord, count);
				} else
					map.put(nowWord, 0);
			}
		}

		System.out.println("Distinct no. of words : " + map.size());
		System.out.println(map);

	}

	public static void main(String[] args) {
		HashMapAssignment hma = new HashMapAssignment(
				"Hi this is a test that is being conducted to find out the multiple occurances of word that is being repeated across a sentence.");
		hma.readWord();

		hma.enterIntoMap(hma.getWord());

	}

}
