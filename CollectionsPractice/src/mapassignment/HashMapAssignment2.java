package mapassignment;

import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;

public class HashMapAssignment2 {

	private SortedMap<State, Integer> map;
	private static Scanner input = new Scanner(System.in);

	void createMap() {

		map = new TreeMap<State, Integer>(new Sorter());

	}

	void populateMap() {
		if (map == null)
			createMap();
		boolean quit = false;
		int choice;
		State state;
		while (!quit) {

			try
			{
			System.out.println("Enter your choice: 1.Create Entry 2.Quit");
			choice = input.nextInt();
			switch (choice) {
			case 1:
				System.out.println("Enter State name,Capital and population");
				state = new State(input.next(), input.next(), input.nextInt());
				map.put(state, state.getPopulation());
				break;
			case 2:
				quit = true;
				break;
			default:
				System.out.println("Wrong choice");
				break;
			}
			}
			catch(InputMismatchException ime)
			{
				System.out.println("Expecting a number here.");
			}
		}

	}

	void sortMap() throws MapNotInitializedException {
		if (map == null)
			throw new MapNotInitializedException();

	}

	void printMap() {
		for (Map.Entry<State, Integer> me : map.entrySet()) {
			System.out.println(me.getKey().getName() + " : " + me.getValue());
		}
	}

}

class Sorter implements Comparator<State> {

	@Override
	public int compare(State o1, State o2) {
		// TODO Auto-generated method stub
		if (o1.getPopulation() == o2.getPopulation()) {
			return o1.getName().compareTo(o2.getName());
		}
		return o1.getPopulation() - o2.getPopulation();
	}

}

class State {
	String name;
	String capital;
	int population;

	public int getPopulation() {
		return population;
	}

	public State(String capital, String name, int population) {
		this.name = name;
		this.capital = capital;
		this.population = population;
	}

	public String getName() {
		return name;
	}

}

class MapNotInitializedException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1667261814845689805L;
	private String message = "The map is not intialized";

	@Override
	public String getMessage() {
		return message;
	}
}
