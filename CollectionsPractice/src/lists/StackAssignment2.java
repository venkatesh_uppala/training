package lists;
import java.util.InputMismatchException;
import java.util.Scanner;


public class StackAssignment2 {

	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) throws ListNotInitializedException {
		boolean quit = false;
		int choice;
		StackAssignment<Integer> sa = new StackAssignment<>();
		try {
			while (!quit) {
				System.out.println("\n1. Add 10 numbers\n2. Add a number at first position\n3. Add a number at last position\n4. Add a number at specific position\n5. Remove first number\n6. Remove last number\n7. Remove number at specific position\n8. Search for a number\n9. Dispsay the list\n0. Exit ");
				System.out.println("Enter you choice:");

				choice = Integer.parseInt(input.next());

				
				switch (choice) {
				case 1:
					System.out.println("Enter 10 numbers");
					Integer[] list = new Integer[10];
					for (int i = 0; i < 10; i++) {
						list[i] = Integer.parseInt(input.next());
					}
					sa.addTen(list);
					break;
				case 2:
					System.out.println("Enter a number");
					sa.addAtStart(input.nextInt());
					break;
				case 3:
					System.out.println("Enter a number");
					sa.addAtLast(input.nextInt());
					break;
				case 4:
					System.out.println("Enter a number following by an index where it is to be added");
					sa.addAt(input.nextInt(), input.nextInt());
					break;
				case 5:
					sa.deleteFirst();
					break;
				case 6:
					sa.deleteLast();
					break;
				case 7:
					System.out.println("Enter the position");
					sa.deleteAt(input.nextInt());
					break;
				case 8:
					System.out.println("Enter the number");
					sa.find(input.nextInt());
					break;
				case 9:
					sa.display();
					break;

				case 0:
					quit = true;
					break;
				default:
					System.out.println("Enter correct choice");
					break;
				}
			}

		}

		 catch (NumberFormatException nfe) {
			System.out.println("Number is expected as input");
		} catch (IllegalArgumentException iae) {
			System.out.println("Illegal arguements");
		}
		catch(InputMismatchException ime)
		{
			System.out.println(ime.getMessage());
		}
		catch(ListNotInitializedException e)
		{
			System.out.println(e.getMessage());
		}
		catch(IndexOutOfBoundsException iobe)
		{
			System.out.println("The index should be between 0 and "+sa.getSize());
		}
		finally {
			input = null;
		}

	}


}
