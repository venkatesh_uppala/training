package lists;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListAssignment<T extends Number> {
	private List<T> numbers = new ArrayList<>();

	private Iterator<T> iterator;

	// Add 10 numbers to list
	// Takes an array of numbers as parameter
	// **************************************

	void checkIntialized() throws ListNotInitializedException {
		if (this.numbers.size() <= 0)
			throw new ListNotInitializedException();
	}

	void addTen(T[] list) {
		if (list.length < 10)
			throw new IllegalArgumentException(
					"Array with 10 numbers is expected");

		for (int i = 0; i < 10; i++) {
			this.numbers.add(list[i]);
		}

	}

	// Add 10 numbers to list
	// Takes a list of numbers as parameter
	// **************************************
	void addTen(List<T> list) {
		if (list == null || list.size() < 10)
			throw new IllegalArgumentException(
					"The parameter object has less than 10 numbers.");
		this.numbers = list;

	}

	// Adds a number at the first position in the list
	// ***********************************************
	void addAtStart(T number) {
		this.numbers.add(0, number);

	}

	// Adds a number at the Last position in the list
	// ***********************************************
	void addAtLast(T number) {
		this.numbers.add(number);

	}

	// Adds a number N at position X in the list.
	// *****************************************
	void addAt(T number, int position) {
		if (position < 0)
			throw new IllegalArgumentException(
					"Value for parameter position should be greater than 0");
		this.numbers.add(position, number);

	}

	// Removes a number at the first position in the list
	// Prints the number that has been deleted
	// **************************************************
	void deleteFirst() throws ListNotInitializedException {
		if (this.numbers == null)
			throw new ListNotInitializedException();
		System.out.println(this.numbers.remove(0)
				+ " has been removed from the list");

	}

	// Removes a number at the last position in the list
	// Prints the number that has been deleted
	// *************************************************
	void deleteLast() throws ListNotInitializedException {
		if (this.numbers == null)
			throw new ListNotInitializedException();
		System.out.println(this.numbers.remove(this.numbers.size() - 1)
				+ " has been removed from the list");
	}

	// Deletes a number at position X in the list.
	// Prints the number that deleted from the list.
	void deleteAt(int position) throws ListNotInitializedException {
		checkIntialized();
		if (position < 0 || position > this.numbers.size() - 1)
			throw new IllegalArgumentException(
					"Position cannot be less than and more than size of actual list");
		System.out.println(this.numbers.remove(position)
				+ " has been removed from the list");

	}

	// Search for all the occurrences of a number X in the list.
	void find(T number) throws ListNotInitializedException {
		checkIntialized();
		iterator = numbers.listIterator();
		int i = 0;

		while (iterator.hasNext()) {

			if (number == iterator.next()) {
				System.out.println("Found at position " + i);

			}
			i++;

		}

	}

	int getSize() {
		return numbers.size();
	}

	void display() throws ListNotInitializedException {
		checkIntialized();
		for (T number : numbers)
			System.out.println(number);
	}
}
