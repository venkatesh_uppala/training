package lists;
import java.util.Enumeration;
import java.util.Random;
import java.util.Vector;


public class VectorExample {
	
	static Vector<Integer> v=new Vector<>();
	public static Enumeration<? extends Number> getEnum()
	{
		v=populate(v);
		Enumeration<? extends Number> e=v.elements();
		return e;
	}
	
	static Vector<Integer> populate(Vector<Integer> v)
	{
		for(int i=0;i<14;i++)
		{
			try {
				v.add(Random.class.newInstance().nextInt(100));
			} catch (InstantiationException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			}
			
		}
		return v;
	}
public static void main(String[] args) 
{
	
	

	v.add(10);
	for(int i=0;i<14;i++)
	{
		try {
			v.add(Random.class.newInstance().nextInt(100));
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
	}
	
	System.out.println("Capacity = "+v.capacity());
	
	System.out.println("Size "+v.size());
	
	System.out.println(v.firstElement());
	
	v.trimToSize();
	
	System.out.println("Capacity after trim = "+v.capacity());
	System.out.println("Size "+v.size());
	
	System.out.println(v.remove(10));
	
	System.out.println(v.remove(11));
	
	System.out.println("Capacity = "+v.capacity());
	System.out.println("Size "+v.size());
	v.trimToSize();
	
	System.out.println("Capacity after trim = "+v.capacity());
	System.out.println("Size "+v.size());
	
	
	
	System.out.println();
	for(Integer i:v)
	{
		System.out.println(i);
	}

}
}
