package lists;

public class ListNotInitializedException extends Exception
{
/**
	 * 
	 */
	private static final long serialVersionUID = 7777683218525235415L;
String message;

@Override
public String getMessage()
{
	return "The List has not been initialized";
}
}
