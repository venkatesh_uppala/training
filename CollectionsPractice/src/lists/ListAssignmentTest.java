package lists;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ListAssignmentTest {

	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) throws ListNotInitializedException {
		boolean quit = false;
		int choice;
		ListAssignment<Integer> la = new ListAssignment<>();
		try {
			while (!quit) {
				System.out.println("\n1. Add 10 numbers\n2. Add a number at first position\n3. Add a number at last position\n4. Add a number at specific position\n5. Remove first number\n6. Remove last number\n7. Remove number at specific position\n8. Search for a number\n9. Display the list\n0. Exit ");
				System.out.println("Enter you choice:");

				choice = Integer.parseInt(input.next());

				
				switch (choice) {
				case 1:
					System.out.println("Enter 10 numbers");
					Integer[] list = new Integer[10];
					for (int i = 0; i < 10; i++) {
						list[i] = Integer.parseInt(input.next());
					}
					la.addTen(list);
					break;
				case 2:
					System.out.println("Enter a number");
					la.addAtStart(input.nextInt());
					break;
				case 3:
					System.out.println("Enter a number");
					la.addAtLast(input.nextInt());
					break;
				case 4:
					System.out.println("Enter a number following by an index where it is to be added");
					la.addAt(input.nextInt(), input.nextInt());
					break;
				case 5:
					la.deleteFirst();
					break;
				case 6:
					la.deleteLast();
					break;
				case 7:
					System.out.println("Enter the position");
					la.deleteAt(input.nextInt());
					break;
				case 8:
					System.out.println("Enter the number");
					la.find(input.nextInt());
					break;
				case 9:
					la.display();
					break;

				case 0:
					quit = true;
					break;
				default:
					System.out.println("Enter correct choice");
					break;
				}
			}

		}

		 catch (NumberFormatException nfe) {
			System.out.println("Number is expected as input");
		} catch (IllegalArgumentException iae) {
			System.out.println("Illegal arguements");
		}
		catch(InputMismatchException ime)
		{
			System.out.println(ime.getMessage());
		}
		catch(ListNotInitializedException e)
		{
			System.out.println(e.getMessage());
		}
		catch(IndexOutOfBoundsException iobe)
		{
			System.out.println("The index should be between 0 and "+la.getSize());
		}
		finally {
			input = null;
		}

	}

}
