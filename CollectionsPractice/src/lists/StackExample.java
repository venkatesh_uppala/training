package lists;
import java.util.Stack;

public class StackExample {

	public static void main(String[] args) {

		Stack<Integer> stack = new Stack<Integer>();
		if(stack.empty())
		System.out.println("Stack is empty");
		
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);

		System.out.println("Elements are " + stack);

		System.out.println("When stack peeks "+stack.peek());
		
		System.out.println("elements of Box are " + stack);


		System.out.println("Position of 5 using search method is "+ stack.search(5));
		System.out.println("removed element from stack : " + stack.pop());
		System.out.println("Elements are "+ stack);

		System.out.println("Position of 2 using search method is "+ stack.search(2));

		System.out.println("empty() returns true if stack is empty  "+ stack.empty());
		System.out.print(stack.get(3));

	}

}
