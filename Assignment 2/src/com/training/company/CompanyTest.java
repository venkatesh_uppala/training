package com.training.company;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CompanyTest {

	static Company company, company1, company2, company3;

	@Before
	public void beforeTest() {

	}

	@After
	public void afterTest() {
		company = null;

	}

	@Test
	public void testCompany() {
		company = new Company(1, "WWE", "World Wrestling Entertainment",
				100000, 49.99, 51.02, 50.00);
		company1 = new Company(2, "WWF", "World Wildlife Federation", 200000,
				19.99, 21.02, 20.00);
		company2 = new Company(3, "WWI", "World Wide Insurance", 300000, 89.99,
				91.02, 90.00);
		company3 = new Company(4, "ORCL", "Oracle", 1000000, 112.2, 113.0,
				112.5);
		double price = 50.00;
		assert (company.getPrice() == price);

	}

	public static Company getCompany() {
		return company;
	}

	public static List<Company> getList() {
		List<Company> companies = new ArrayList<>();
		companies.add(company);
		companies.add(company1);
		companies.add(company2);
		companies.add(company3);
		return companies;

	}

}
