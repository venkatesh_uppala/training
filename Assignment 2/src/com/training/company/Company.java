package com.training.company;


public class Company {

	private long id;
	private String symbol;
	private String name;
	private long volume;
	private double previouslyClosed;
	private double openingPrice;
	private double price;

	public Company(long id, String symbol, String name, long volume,
			double previouslyClosed, double openingPrice, double price) {
		if (id < 0 || (symbol.equals("") || symbol == null) || volume < 0)
			throw new IllegalArgumentException(
					"FIELDS ARE NOT INITIALIZED PROPERLY");
		this.id = id;
		this.symbol = symbol;
		this.name = name;
		this.volume = volume;
		this.previouslyClosed = previouslyClosed;
		this.openingPrice = openingPrice;
		this.price = price;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null && !(obj instanceof Company))
			return false;
		Company company = (Company) obj;
		return ( (this.getName().equals(company.getName())) && this.getSymbol().equals(company.getSymbol()));
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPreviouslyClosed() {
		return previouslyClosed;
	}

	public void setPreviouslyClosed(double previouslyClosed) {
		this.previouslyClosed = previouslyClosed;
	}

	public double getOpeningPrice() {
		return openingPrice;
	}

	public void setOpeningPrice(double openingPrice) {
		this.openingPrice = openingPrice;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public long getId() {
		return id;
	}

	public String getSymbol() {
		return symbol;
	}

	public long getVolume() {
		return volume;
	}
}
