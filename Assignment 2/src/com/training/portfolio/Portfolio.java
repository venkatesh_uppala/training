package com.training.portfolio;

import java.util.ArrayList;
import java.util.List;

import com.training.company.Company;
import com.training.user.User;

public class Portfolio {
	private long id;
	private String name;
	private User user;
	private List<Company> companies;

	public Portfolio(long id, String name, User user, List<Company> companies) {
		if(id<0 || user==null || companies==null)
			throw new IllegalArgumentException("Please pass the parameters which are initialized");
		this.id = id;
		this.name = name;
		this.user = user;
		this.companies = companies;
	}

	public Portfolio(long id, String name, User user, Company company) {
		if(id<0 || user==null || company==null)
			throw new IllegalArgumentException("Please pass the parameters which are initialized");
		this.id = id;
		this.name = name;
		this.user = user;
		this.companies = new ArrayList<>();
		companies.add(company);
	}
	
	public Portfolio(long id,String name,User user)
	{
		if(id<0 || user==null)
			throw new IllegalArgumentException("Please pass the parameters which are initialized");
		this.id = id;
		this.name = name;
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Company> getCompanies() {
		if(companies==null)
			throw new NullPointerException("The companies may not have been intialized");
		return companies;
	}

	public void setCompanies(List<Company> companies) {
		if(companies==null)
			throw new NullPointerException("The parameter has null reference");
		this.companies = companies;
	}

	public void addCompanies(Company company) {
		if (this.companies == null)
			this.companies = new ArrayList<>();
		companies.add(company);
	}

	public long getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public void print() {
		
		System.out.println(user.getName() + "\n\n****************************"
				+ this.name + "***************************\n\n");
		System.out.println("Name \t\t\t\t Symbol \t Last closed \t Opened at");
		for (Company company : companies) {
			System.out.println(company.getName() + " \t " + company.getSymbol()
					+ " \t \t  " + company.getPreviouslyClosed() + " \t \t"
					+ company.getOpeningPrice());
		}
	}

}
