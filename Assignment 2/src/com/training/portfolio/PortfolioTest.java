package com.training.portfolio;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import com.training.company.Company;
import com.training.company.CompanyTest;
import com.training.user.User;

public class PortfolioTest {

	static Portfolio portfolio, portfolio1;

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testPortfolioLongStringUserListOfCompany() {
		List<Company> companies = CompanyTest.getList();
		
		portfolio = new Portfolio(2, "My Port", new User("SherDilTakesSelfie",
				"hello", "Sher Dil", "sherdil@savetigers.com", "Male"),
				companies.get(0));
		
		portfolio1 = new Portfolio(3, "My Port1", new User("PuLiTakesSelfie",
				"Yooo", "PuLi", "PuLi@savetigers.com", "Male"), companies);
		
		portfolio.addCompanies(companies.get(1));
		portfolio.addCompanies(companies.get(2));

		assert (portfolio.getUser().getName().equals("Sher Dil"));
	}

	@Test
	public void testPortfolioLongStringUserCompany() {
		CompanyTest ct = new CompanyTest();
		ct.testCompany();
		Portfolio port = new Portfolio(4, "My port part 2", new User(
				"SherDilTakesSelfie", "hello", "Sher Dil",
				"sherdil@savetigers.com", "Male"), CompanyTest.getCompany());
		if (port.getCompanies().get(0) != null)
			assert (port.getCompanies().get(0).getName().equals(CompanyTest
					.getCompany().getName()));
	}

	public Portfolio getPortfolio() {
		return portfolio;
	}

	public List<Portfolio> getPortList() {
		List<Portfolio> portfolios = new ArrayList<Portfolio>();
		portfolios.add(portfolio);
		portfolios.add(portfolio1);
		return portfolios;
	}
}
