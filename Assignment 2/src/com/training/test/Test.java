package com.training.test;

import java.util.List;

import com.training.company.Company;
import com.training.company.CompanyTest;
import com.training.portfolio.Portfolio;
import com.training.portfolio.PortfolioTest;
import com.training.user.User;
import com.training.user.UserTest;

public class Test {

	public void find(User user, Company company) {
		if(user==null || company==null)
			throw new NullPointerException("The parameters may not have been intialized");
		boolean found = false;
		PortfolioTest pt = new PortfolioTest();
		CompanyTest ct = new CompanyTest();
		ct.testCompany();
		pt.testPortfolioLongStringUserListOfCompany();
		List<Portfolio> portfolios = pt.getPortList();
		for (Portfolio portfolio : portfolios) {
			if (portfolio.getUser().equals(user)) {
				for (Company portCompany : portfolio.getCompanies()) {
					if (portCompany.equals(company)) {
						System.out.println(portCompany.getName()
								+ " found in portfolio " + portfolio.getName()
								+ " belonging to user " + user.getName());
						found = true;
						break;
					}

				}
			}
		}
		if (!found)
			System.out.println("No such company found");
	}

	public static void main(String[] args) {

		UserTest ut = new UserTest();

		Test t = new Test();
		t.find(ut.getUser(), new Company(4, "ORCL", "Oracle", 1000000, 112.2,
				113.0, 112.5));

	}

}
