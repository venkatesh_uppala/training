package com.training.user;

public class User {
	private String userId;
	private String password;
	private String name;
	private String email;
	private String gender;

	public User(String userId, String password, String name, String email,
			String gender) {
		if(userId==null || password==null || name==null || email==null || userId=="" || password=="" || name=="" || email=="")
			throw new IllegalArgumentException("Please pass the parameters which are initialized");
		this.userId = userId;
		this.password = password;
		this.name = name;
		this.email = email;
		this.gender = gender;
	}
	@Override
	public boolean equals(Object obj)
	{
		if(obj==null && !(obj instanceof User))
			return false;
		User user=(User)obj;
		return this.getName().equals(user.getName());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getUserId() {
		return userId;
	}

	public String getPassword() {
		return password;
	}

}
