package com.training.user;

import org.junit.After;
import org.junit.Test;

public class UserTest {

	protected static User user;

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testUser() {
		String email = "sherdil@savetigers.com";
		user = new User("SherDilTakesSelfie", "hello", "Sher Dil",
				"sherdil@savetigers.com", "Male");
		assert (user.getEmail().equals(email));
	}

	public static User getUser() {
		return new User("PuLiTakesSelfie", "Yooo", "PuLi",
				"PuLi@savetigers.com", "Male");
	}

}
