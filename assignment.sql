CREATE TABLE training.community
	(
	id int not null PRIMARY KEY auto_increment,
	name varchar(30),
	description varchar(500),
	address varchar(400),
	city varchar(40),
	state varchar(40),
	pincode varchar(15)			
	); 

CREATE TABLE training.vendor
	(
	id int not null PRIMARY KEY auto_increment,
	name varchar(30),
	description varchar(500),
	URL varchar(40),
	location varchar(200),
	email varchar(40),
	phonenumber numeric(15)	
	); 
CREATE TABLE training.event
	(
	id int not null PRIMARY KEY auto_increment,
	short_description varchar(100),
	description varchar(500),
	start_date date,
	end_date date,
	community_id int not null,
	vendor_id int not null,
	FOREIGN KEY (community_id) REFERENCES community(id),
	FOREIGN KEY (vendor_id) REFERENCES vendor(id)
	);

INSERT 
	INTO training.community
	(
		id,
		name,
		description,
		address,
		city,
		state,
		pincode
	) 
			
	VALUES
	(
		1,
		"Shilpa Kala Vedika",
		"Events are conducted here",
		"Kondapur",
		"Hyderabad",
		"Andhra Pradesh",
		"500001"
	);


INSERT 
	INTO training.community
	(
		id,
		name,
		description,
		address,
		city,
		state,
		pincode
	) 
	
	VALUES
	(
		2,
		"Hitex",
		"Events are also conducted here",
		"Kondapur part II",
		"Hyderabad",
		"Andhra Pradesh",
		"500002"
	);


INSERT 
	INTO training.community
	(
		id,
		name,
		description,
		address,
		city,
		state,
		pincode
	) 
	VALUES
	(
		3,
		"Another community",
		"Yet we also conduct events",
		"Kondapur part III",
		"Hyderabad",
		"Andhra Pradesh",
		"500003"
	);

INSERT 
	INTO training.vendor
	(
		id,
		name,
		description,
		URL,
		location,
		email,
		phonenumber
	) 
	VALUES
	(
		1,
		"Hitachi",
		"We are INTO everything",
		"www.hitachi.com",
		"Hyderabad",
		"mail_me@hitachi.com",
		9002233445
	);

INSERT 
	INTO training.vendor
	(
		id,
		name,
		description,
		URL,
		location,
		email,
		phonenumber
	) 
	VALUES
	(
		2,
		"Dell",
		"We are INTO computers",
		"www.dell.com",
		"Hyderabad",
		"mail_me@dell.com",
		9014055668
	);

INSERT 
	INTO training.vendor
	(
		id,
		name,
		description,
		URL,
		location,
		email,
		phonenumber
	) 
	VALUES
	(
		3,
		"Nokia",
		"We are doomed",
		"www.nokia.com",
		"Hyderabad",
		"stop_mailing_me@nokia.com",
		9052357800
	);

INSERT 
	INTO training.event
	(
		id,
		short_description,
		description,
		start_date,
		end_date,
		community_id,
		vendor_id
	) 
	VALUES
	(
		1,
		"Launching a new mobile",
		"So much to say but little time",
		'2014-07-05',
		'2014-07-08',
		2,
		1
	);
INSERT 
	INTO training.event
	(
		id,
		short_description,
		description,
		start_date,end_date,
		community_id,vendor_id
	) 
	VALUES
	(
		2,
		"Launching a new alienware PC",
		"It is awesome",
		'2014-07-09',
		'2014-07-10',
		2,
		2
	);



CREATE TABLE college(id int not null PRIMARY KEY auto_increment,name varchar(40),address varchar(100),url varchar(40),telephone numeric);

CREATE TABLE department(id int not null, name varchar(30),col_id int,FOREIGN KEY (col_id) REFERENCES college(id));

alter TABLE training.department add PRIMARY KEY(id);

CREATE TABLE course(id int not null PRIMARY KEY auto_increment,name varchar(30),duration numeric,specialization varchar(40),type varchar(40),col_id int, d_id int,FOREIGN KEY (col_id) REFERENCES college(id), FOREIGN KEY (d_id) REFERENCES department(id));

CREATE TABLE teachers(id int not null PRIMARY KEY auto_increment, name varchar(30),d_id int,FOREIGN KEY (d_id) REFERENCES department(id));

CREATE TABLE class(id int not null PRIMARY KEY auto_increment, name varchar(30),course_id int,t_id int,FOREIGN KEY (course_id) REFERENCES course(id),FOREIGN KEY (t_id) REFERENCES teachers(id));

CREATE TABLE student(id int not null PRIMARY KEY auto_increment,name varchar(50),date_of_joining date,address varchar(100),DOB date,col_id int,course_id int,d_id int,FOREIGN KEY (col_id) REFERENCES college(id),FOREIGN KEY (course_id) REFERENCES course(id),FOREIGN KEY (d_id) REFERENCES department(id));

CREATE TABLE enrollment(id int not null PRIMARY KEY auto_increment,s_id int not null, class_id int not null, FOREIGN KEY (s_id) REFERENCES student(id),FOREIGN KEY (class_id) REFERENCES class(id));


INSERT INTO college(id,name,address,url,telephone) VALUES(1,"HITS","Bogaram","www.hits.ac.in",040556677);
INSERT INTO college(id,name,address,url,telephone) VALUES(2,"HIT","Bogaram","www.hits.ac.in",040556677);
INSERT INTO college(id,name,address,url,telephone) VALUES(3,"BITS","Secunderabad","www.bits.ac.in",040555555);

INSERT INTO department(id,name) VALUES(1,"CSE");
INSERT INTO department(id,name) VALUES(2,"IT");
INSERT INTO department(id,name) VALUES(3,"ECE");

INSERT INTO course(id,name,specialization,duration,type,col_id,d_id) VALUES(1,"B.tech(CSE)","Computers",48,"Full time",2,1);
INSERT INTO course(id,name,specialization,duration,type,col_id,d_id) VALUES(2,"B.tech(IT)","Information tech",48,"Full time",1,1);
INSERT INTO course(id,name,specialization,duration,type,col_id,d_id) VALUES(3,"B.tech(ECE)","Electronics",48,"Full time",3,1);

INSERT INTO teachers(id,name,d_id) VALUES(1,"PuLi",2);
INSERT INTO teachers(id,name,d_id) VALUES(2,"Simham",3);
INSERT INTO teachers(id,name,d_id) VALUES(3,"Pilli",1);

INSERT INTO class(id,name,course_id,t_id) VALUES(1,"C++ OOPS",2,1);
INSERT INTO class(id,name,course_id,t_id) VALUES(2,"JAVA",1,3);
INSERT INTO class(id,name,course_id,t_id) VALUES(3,"ES",3,2);

INSERT INTO student(id,name,date_of_joining,address,DOB,col_id,course_id,d_id) VALUES(1,"Venki",'2010-06-06',"Moulali",'1990-02-24',1,2,2);
INSERT INTO student(id,name,date_of_joining,address,DOB,col_id,course_id,d_id) VALUES(2,"Adil",'2010-06-06',"Matkesar",'1992-01-14',2,1,1);

INSERT INTO enrollment(id,s_id,class_id) VALUES(1,1,1);
INSERT INTO enrollment(id,s_id,class_id) VALUES(2,2,3);

SELECT * FROM student;
UPDATE student SET address="APHB colony" WHERE id=1;
SELECT * FROM student;

SELECT * FROM teachers;
UPDATE teachers SET name="Khadga Mrugam" WHERE id=3;
SELECT * FROM teachers;


INSERT INTO class(id,name,course_id,t_id) VALUES(4,"P&S",3,2);

SELECT * FROM class;

DELETE FROM class WHERE id=4;

SELECT * FROM class;



CREATE TABLE training.compositeExample(name varchar(30),fathers_name varchar(30),address varchar(30),hometown varchar(20), PRIMARY KEY (name,fathers_name));


DROP TABLE compositeExample;




