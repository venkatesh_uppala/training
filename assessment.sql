CREATE DATABASE assessment;
USE assessment;
CREATE TABLE User
				(
				userId VARCHAR(50) PRIMARY KEY NOT NULL,
				password VARCHAR(50) NOT NULL,
				age INT,
				gender VARCHAR(8),
				alternateEmail VARCHAR(50)
				) engine=innoDB;
				
CREATE TABLE Portfolio
				(
				id INT NOT NULL PRIMARY KEY,
				name VARCHAR(50),
				user_id VARCHAR(50) NOT NULL,
				FOREIGN KEY (user_id) REFERENCES User(userId)
				);

CREATE TABLE Company
				(
				id INT NOT NULL PRIMARY KEY,
				name VARCHAR(50),
				symbol VARCHAR(4) NOT NULL UNIQUE,
				volume BIGINT);

CREATE TABLE PortfolioHasCompany
				(
				portfolio_id INT NOT NULL,
				company_id INT NOT NULL,
				FOREIGN KEY (portfolio_id) REFERENCES Portfolio(id),
				FOREIGN KEY (company_id) REFERENCES Company(id)
				);


CREATE TABLE Stock
				(
				date DATE NOT NULL PRIMARY KEY,
				openingPrice DECIMAL,
				previouslyClosed DECIMAL,
				company_id INT NOT NULL,
				FOREIGN KEY (company_id) REFERENCES Company(id)
				);

CREATE TABLE Value
				(
				time TIME NOT NULL,
				stock_date DATE NOT NULL,
				value DECIMAL,
				currentValues DECIMAL NOT NULL,
				PRIMARY KEY(time,stock_date),
				FOREIGN KEY (stock_date) REFERENCES Stock(date)
				);
				

--2nd assessment
use test;
CREATE TABLE User
(
   uid           VARCHAR(50) NOT NULL PRIMARY KEY,
   password      VARCHAR(50) NOT NULL,
   email         VARCHAR(50),
   phoneNumber   INT
);

CREATE TABLE Company
(
   id            INT NOT NULL PRIMARY KEY,
   name          VARCHAR(50),
   established   DATE,
   industry      VARCHAR(50)
);

CREATE TABLE Individual
(
   id                     INT NOT NULL PRIMARY KEY,
   name                   VARCHAR(50),
   age                    INT,
   gender                 VARCHAR(6),
   highestQualification   VARCHAR(50),
   user_id                VARCHAR(30) ,
   FOREIGN KEY (user_id) REFERENCES User(uid)
);


CREATE TABLE jobPost
(
   id                 INT NOT NULL PRIMARY KEY,
   shortDescription   VARCHAR(45) NOT NULL,
   longDesc           VARCHAR(200),
   payMin             DECIMAL,
   payMax             DECIMAL,
   deadline           DATETIME,
   company_id         INT,
   FOREIGN KEY(company_id) REFERENCES Company(id)
);


CREATE TABLE Subscription
(
   individual_id   INT,
   company_id      INT,
   FOREIGN KEY(individual_id) REFERENCES Individual(id),
   FOREIGN KEY(company_id) REFERENCES Company(id)
);

CREATE TABLE User_Company
(
   user_id      VARCHAR(30),
   company_id   INT,
   FOREIGN KEY(user_id) REFERENCES User(uid),
   FOREIGN KEY(company_id) REFERENCES Company(id)
);

CREATE TABLE Profile
(
   id              INT NOT NULL PRIMARY KEY,
   individual_id   INT,
   FOREIGN KEY(individual_id) REFERENCES Individual(id)
);

CREATE TABLE Bid
(
   id               INT NOT NULL PRIMARY KEY,
   effort           VARCHAR(50),
   amount           DECIMAL,
   completionDate   DATE,
   startDate        DATE,
   jobPost_id       INT,
   profile_id       INT,
   individual_id    INT,
   FOREIGN KEY(jobPost_id) REFERENCES JobPost(id),
   FOREIGN KEY(profile_id) REFERENCES Profile(id),
   FOREIGN KEY(individual_id) REFERENCES Individual(id)
);

CREATE TABLE Skill
(
   individual_id   INT,
   profile_id      INT,
   FOREIGN KEY(profile_id) REFERENCES Profile(id),
   FOREIGN KEY(individual_id) REFERENCES Individual(id),
   skill           VARCHAR(70)
);

CREATE TABLE Qualification
(
   individual_id   INT,
   profile_id      INT,
   FOREIGN KEY(profile_id) REFERENCES Profile(id),
   FOREIGN KEY(individual_id) REFERENCES Individual(id),
   qualification   VARCHAR(70)
);
