
public class StringBuilderExample {

	public static void main(String[] args)
	{
		StringBuilder sb=new StringBuilder("PuLi");
		StringBuilder sb1=sb;
		sb1.append(" in CS");
		sb.append(" is pro");
		System.out.println(sb);
		System.out.println(sb1);
		System.out.println(sb1.capacity());
		System.out.println(sb1.charAt(9));
		System.out.println(sb1.indexOf("pro", 6));
		sb1.setCharAt(9, 'T');
		System.out.println(sb);	
		System.out.println(sb.reverse());	
	}
}
