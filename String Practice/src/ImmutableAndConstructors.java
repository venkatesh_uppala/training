import java.io.UnsupportedEncodingException;


public class ImmutableAndConstructors {
public static void main(String[] args) throws UnsupportedEncodingException
{
	//********IMMUTABLE********//
	/*String s="ABC";
	String s1=s;
	System.out.println((s==s1));
	
	System.out.println((s==s1));
	System.out.println((s));
	System.out.println((s1));
	if(s.concat("D").equals("ABCD"))
	System.out.println(("ABCD"));
	*/
	
	//********CONSTRUCTOR********//
	char[] c={'a','b','c','d'};
	String s=new String(c);
	System.out.println(new String(c,1,3));
	byte[] b={65,66,67,68,69,70};
	String s1=new String(b);
	System.out.println(s);
	System.out.println(s1);
	System.out.println(new String(b,2,3));
	System.out.println(new String(b,2,3,"UTF-8"));
	
	
	
}
}
