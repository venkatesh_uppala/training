package AssignmentAnnotations;

public class NumbersTest {

	@Test(number = 1)
	boolean testMax(int a, int b) {
		System.out.println("In \"testMax\" method");
		return a > b;
	}

	@Test(number = 2)
	boolean testMin(int a, int b) {
		System.out.println("In \"testMin\" method");
		return a < b;
	}

	@Test(number = 3)
	boolean testZero(int a) {
		System.out.println("In \"testZero\" method");
		return a == 0;
	}

}
