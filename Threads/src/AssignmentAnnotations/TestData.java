package AssignmentAnnotations;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class TestData {

	public static void main(String[] args) {

		NumbersTest numTest = new NumbersTest();
		for (Method m : NumbersTest.class.getDeclaredMethods()) {
			try {
				System.out.println("Executing Method:\""
						+ m.getName()
						+ "\""
						+ "\n"
						+ m.getName()
						+ ":"
						+ (m.getParameterTypes().length > 1 ? m.invoke(numTest,
								2, 3) : m.invoke(numTest, 2))
						+ "\ntest number : "
						+ m.getAnnotation(Test.class).number());
			} catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
