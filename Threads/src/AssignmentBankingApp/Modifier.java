package AssignmentBankingApp;

import java.util.Random;

public class Modifier extends Thread {

	Account account;
	String name;
	double total;
	double temp;
	volatile static double fullAmount;
	public static double getFullAmount() {
		return fullAmount;
	}

	public Modifier(Account account, String name) {
		this.account = account;
		this.name = name;
	}

	@Override
	public void run() {

		double initial = account.getBalance();
		debtCollection();
		creditSalary();
		deductTaxes();
		if (account.getBalance() == initial + total) {
			System.out.println("(y) Done.");
		}
		fullAmount+=total;
		System.out
				.println("Total amount that is added/deducted by " + this.name
						+ " is =" + total
						);

	}

	public synchronized void debtCollection() {
		temp = getDouble();
		System.out.println("\n\n"+name + " is collecting credit card bills of Rs."+ temp);
		account.debit(temp);
		total -= temp;
		

	}

	public synchronized void creditSalary() {
		temp = getDouble();
		System.out.println(name + " is crediting salary.... Rs." + temp);
		account.credit(temp);
		total += temp;
		
	}

	public synchronized void deductTaxes() {
		temp = getDouble();
		System.out.println("Government is deducting taxes of Rs." + temp+ " from your account");
		account.debit(temp);
		total -= temp;
		
	}

	public synchronized int getDouble() {
		try {
			return Random.class.newInstance().nextInt(5000);
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
}
