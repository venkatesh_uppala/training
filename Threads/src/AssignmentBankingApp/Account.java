package AssignmentBankingApp;

public class Account {
	
	private volatile double balance;
	
	public synchronized double getBalance() {
		return balance;
	}
	public Account(double balance)
	{
		this.balance=balance;
	}
	public synchronized void credit(double amount)
	{
		balance+=amount;
	}
	
	public synchronized void debit(double amount)
	{
		balance-=amount;
	}
	

}
