package AssignmentBankingApp;

public class TestData {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Account account = new Account(100000.0);
		System.out.println("Intial balance =" + account.getBalance());
		Modifier modifier = new Modifier(account, "Adil");
		Modifier modifier1 = new Modifier(account, "DraghV");
		Modifier modifier2 = new Modifier(account, "SabJiya");
		Modifier modifier3 = new Modifier(account, "Saenchit");
		Modifier modifier4 = new Modifier(account, "Sher Dil");
		modifier.start();
		modifier1.start();
		modifier2.start();
		modifier3.start();
		modifier4.start();

		try {
			modifier.join();
			modifier1.join();
			modifier2.join();
			modifier3.join();
			modifier4.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("\n\nTotal amount that has been transacted is ="+Modifier.getFullAmount());
		System.out.println("Final balance =" + account.getBalance());
	}

}
