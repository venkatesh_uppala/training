package AssignmentMarket;

public class TestData {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Market market=new Market(4,0);

		Farmer farmer=new Farmer(10, 10, 10, 10, "Sher dil",market);
		Farmer farmer1=new Farmer(7, 9, 11, 13, "PuLi",market);
		Farmer farmer2=new Farmer(8, 10, 12, 14, "DraghV",market);
		Farmer farmer3=new Farmer(6, 8, 10, 12, "Sabjiya",market);
	
		Consumer consumer=new Consumer(10, 10, 10, 10,"Adil",market);
		Consumer consumer1=new Consumer(5, 5, 5, 5,"Venki",market);
		Consumer consumer2=new Consumer(8, 7, 9, 9,"RaghV",market);
		
		farmer.start();
		farmer1.start();
		farmer2.start();
		farmer3.start();
		consumer.start();
		consumer1.start();
		consumer2.start();
		
	}

}
