package AssignmentMarket;

import java.util.ArrayList;
import java.util.List;

public class Consumer extends Thread {

	List<Integer> requirement = new ArrayList<>();

	String name;
	Market market;

	public Consumer(int apple, int orange, int grape, int watermelon,
			String name, Market market) {

		super(name);
		requirement.add(apple);
		requirement.add(orange);
		requirement.add(grape);
		requirement.add(watermelon);
		this.market = market;
	}

	public void run() {
		buy();
	}

	public synchronized void buy() {
		System.out.println();
		while (!(allEmpty(requirement) && market.queueFarmer.isEmpty())) {

			for (Farmer farmer : market.queueFarmer) {
				
					List<Integer> basket = farmer.getBasket();
					for (int i = 0; i < requirement.size(); i++) {

						int customerReq = requirement.get(i), fruitCount = basket
								.get(i);

						if (customerReq != 0)
							if (customerReq == fruitCount) {
								basket.set(i, 0);
								requirement.set(i, 0);
								System.out.println(this.getName() + " bought "
										+ customerReq + " fruits from "
										+ farmer.getName());
							} else if (customerReq < fruitCount) {
								basket.set(i, fruitCount - customerReq);
								requirement.set(i, 0);
								System.out.println(this.getName() + " bought "
										+ customerReq + " fruits from "
										+ farmer.getName());
							} else {
								requirement.set(i, customerReq - fruitCount);
								basket.set(i, 0);
								System.out.println(this.getName() + " bought "
										+ customerReq + " fruits from "
										+ farmer.getName());
							}

					

				}

				if (!allEmpty(requirement))
					try {
						System.out.println("Market ran out of stock. "
								+ this.getName() + " is waiting........");
						this.wait(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				else
					break;
			}
		}

			if (allEmpty(requirement))
				System.out.println(this.getName()
						+ " is leaving market, he got what he wanted.");
			else
				System.out.println(this.getName()
						+ " is leaving market,couldnt find fruits.");

		
	}

	boolean allEmpty(List<Integer> list) {
		boolean empty = true;
		for (Integer i : list) {
			if (i != 0)
				empty = false;
		}

		return empty;
	}

}
