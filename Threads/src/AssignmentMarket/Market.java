package AssignmentMarket;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Market {

	int capacity;
	int occupiedSpots;
	BlockingQueue<Farmer> queueFarmer;
	Consumer consumer;

	public Market(int capacity, int spotsOccupied) {
		this.capacity = capacity;
		this.occupiedSpots = spotsOccupied;
		queueFarmer = new LinkedBlockingQueue<>(capacity);

	}

	public void getSpot(Farmer farmer) {

		addToFarmerQueue(farmer);

		occupiedSpots++;
		System.out.println("Spot no. " + occupiedSpots
				+ " has been occupied by " + farmer.getName());

	}

	public void enterMarket(Consumer consumer) {
		this.consumer = consumer;
	}

	public void releaseSpot(Farmer farmer) {

		try {
			System.out.println("Spot no. " + occupiedSpots
					+ " has been released by " + queueFarmer.take().getName());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		occupiedSpots--;

	}

	private synchronized void addToFarmerQueue(Farmer farmer) {
		try {
			queueFarmer.put(farmer);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
