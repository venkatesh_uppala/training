package AssignmentMarket;

import java.util.ArrayList;
import java.util.List;

public class Farmer extends Thread {

	volatile List<Integer> basket = new ArrayList<>();

	public synchronized List<Integer> getBasket() {
		return basket;
	}

	String name;

	Market market;

	public Farmer(int apple, int orange, int grape, int watermelon,
			String name, Market market) {

		super(name);
		basket.add(apple);
		basket.add(orange);
		basket.add(grape);
		basket.add(watermelon);
		this.market = market;

	}

	/*
	 * public void sellFruit(String fruitname,int quantity) {
	 * 
	 * String fruit=fruitname.toLowerCase(); if(basket.containsKey(fruit)) {
	 * if(quantity<=basket.get(fruit)) { basket.put(fruit,
	 * basket.get(fruit)-quantity); } else try { wait(); } catch
	 * (InterruptedException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } } }
	 */
	public void run() {

		market.getSpot(this);
		long start = System.currentTimeMillis();
		while (!allEmpty(basket)) {

			try {
				sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (System.currentTimeMillis() - start > 5000) {
				if (allEmpty(basket))
					System.out
							.println(this.getName()
									+ " is leaving market, couldnt sell all fruits");
				else
					System.out.println(this.getName()
							+ " is leaving market, sold off all fruits");
				break;
			}
		}
		market.releaseSpot(this);

	}

	boolean allEmpty(List<Integer> list) {
		boolean empty = true;
		for (Integer i : list) {
			if (i != 0)
				empty = false;
		}

		return empty;
	}

}
