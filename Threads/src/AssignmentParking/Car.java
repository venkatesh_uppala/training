package AssignmentParking;

public class Car extends Thread {

	ParkingGarage garage;
	String name;
	
	public Car(ParkingGarage garage,String name)
	{
		this.garage=garage;
		this.name=name;
	}
	
	@Override
	public void run()
	{
		
			garage.enter();
			try {
				sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			garage.leave();
		
		
	}
	
	
}
