package AssignmentParking;


public class ParkingGarage {
	
	int numOfSpots,capacity,name;
	public ParkingGarage(int capacity,int spotsFilled)
	{
		this.numOfSpots=capacity-spotsFilled;
		this.capacity=capacity;
	}
	
	public synchronized void enter()
	{
		while(numOfSpots<=0)
		{
			System.out.println("No parking spot available. Waiting.....");
			try {
				this.wait(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			System.out.println("Parking spot no. "+numOfSpots+" has been occupied.");
			numOfSpots--;
		
		
		
		
	}
	
	public synchronized void leave()
	{
		if(numOfSpots<capacity)
		{
			numOfSpots++;
			System.out.println("A parking spot has been released.");
			notifyAll();
		}
		else
		{
			System.out.println("No vehicles remaining. All spots are free to occupy.");
		}
		
	}
	
	public boolean hasSpot()
	{
		return numOfSpots>0;
	}

	
	
}
