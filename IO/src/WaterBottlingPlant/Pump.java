package WaterBottlingPlant;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Pump extends Thread {

	DataOutputStream pump;

	public Pump(OutputStream out) {
		pump = new DataOutputStream(out);
	}

	synchronized void pumpToBottles() {
		System.out.println("Pump is loading bottles into carrier");

		for (int i = 0; i < 75; i++) {

			try {

				pump.write(i);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		notifyAll();
		System.out.println("Pump is going to fill more bottles");

	}

	@Override
	public void run() {

		for (int i = 0; i < 4; i++) {
			System.out.println("Pump is ready to fill");
			pumpToBottles();
		}

	}

}
