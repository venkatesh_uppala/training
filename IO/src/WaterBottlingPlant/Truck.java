package WaterBottlingPlant;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Truck extends Thread {

	DataInputStream truck;

	public Truck(InputStream in) {
		truck = new DataInputStream(in);
	}

	synchronized void loadTruck() {
		System.out.println("Truck is being loaded....");
		try {

			for (int i = 0; i < 75; i++) {
				truck.read();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Truck has dropped off all bottles");
		notifyAll();
	}

	@Override
	public void run() {
		for (int i = 0; i < 4; i++) {
			System.out.println("Truck is ready to get loaded");
			loadTruck();
		}

	}

}
