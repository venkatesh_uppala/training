package FileAssignment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CopyFile {

	
	File openFile(String uri)
	{
	return new File(uri);	
		
	}
	
	void copyFile(File file)
	{
	
		if(file.canRead())
		{
			String path=file.getAbsolutePath();
			File newFile=new File(path.substring(0, path.length()-4)+"copy.txt");
			
			BufferedReader reader;
			BufferedWriter writer;
			try {
				
				reader=new BufferedReader(new FileReader(file));
				writer=new BufferedWriter(new FileWriter(newFile));
				String line;
				while((line=reader.readLine())!=null)
				{
					
					writer.newLine();
					writer.write(line);
				}
				writer.close();
				reader.close();
			} catch (FileNotFoundException ex) {
				// TODO Auto-generated catch block
				System.out.println("The specified file doesn't exist.");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.getMessage();
				e.printStackTrace();
			}
			}
		else
			System.out.println("Cant do it");
	}
	
}
