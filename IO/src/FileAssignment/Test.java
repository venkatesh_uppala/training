package FileAssignment;

import java.io.File;

public class Test {

	public static void main(String[] args) {
		// Copying to file
		FileManipulation fm = new FileManipulation();
		File file = fm.openFile("D:\\test.txt");
		fm.copyFile(file);
		// Replacing Chars
		File file1 = fm.openFile("D:\\testcopy.txt");

		fm.replaceCharsOfFile(file, file1, ' ', '@');
		// Splitting into two files
		File source = fm.openFile("D:\\yo.txt");
		File dest1 = fm.openFile("D:\\yo1.txt");
		File dest2 = fm.openFile("D:\\yo2.txt");
		WriteToTwoFiles two = new WriteToTwoFiles(source);
		two.splitToFiles(dest1, dest2);

	}

}
