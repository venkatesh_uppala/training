package FileAssignment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class WriteToTwoFiles {
	File file;

	public WriteToTwoFiles() {

	}

	public WriteToTwoFiles(File file) {
		this.file = file;
	}

	void setFile(File file) {
		this.file = file;
	}

	public File getFile() {
		return file;
	}

	void splitToFiles(File file1, File file2) {
		if (file != null) {
			try {
				writeAlternateLines(new BufferedWriter(new FileWriter(file1)),
						new BufferedReader(new FileReader(this.file)), 0);
				writeAlternateLines(new BufferedWriter(new FileWriter(file2)),
						new BufferedReader(new FileReader(this.file)), 1);

			} catch (FileNotFoundException e) {
				System.out.println("File not found");
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		else
			System.out.println("\nfInitialize the file please.");
	}

	void splitToFiles(File file, File file1, File file2) {
		if (file != null) {
			try {
				this.file = file;

				writeAlternateLines(new BufferedWriter(new FileWriter(file1)),
						new BufferedReader(new FileReader(this.file)), 0);
				writeAlternateLines(new BufferedWriter(new FileWriter(file2)),
						new BufferedReader(new FileReader(this.file)), 1);

			} catch (FileNotFoundException e) {
				System.out.println("File not found");
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		else
			System.out.println("\nfInitialize the file please.");
	}

	private void writeAlternateLines(BufferedWriter writer,
			BufferedReader reader, int pos) {
		String line = null;
		try {

			if (pos == 0)

				while ((line = reader.readLine()) != null) {
					writer.write(line);
					writer.newLine();
					if (reader.readLine() == null)
						break;
				}
			else
				while ((reader.readLine()) != null) {

					if ((line = reader.readLine()) != null)
						writer.write(line);
					writer.newLine();
				}
			writer.flush();
			writer.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
