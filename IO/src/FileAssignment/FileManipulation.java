package FileAssignment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileManipulation {

	private BufferedReader reader;
	private BufferedWriter writer;

	public File openFile(String uri) {
		File file = new File(uri);
		if (!file.exists())
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return file;

	}

	void copyFile(File file) {

		if (file.canRead()) {
			String path = file.getAbsolutePath();
			File newFile = new File(path.substring(0, path.length() - 4)
					+ "copy.txt");

			BufferedReader reader;
			BufferedWriter writer;
			try {

				reader = new BufferedReader(getFileReader(file));
				writer = new BufferedWriter(getFileWriter(newFile));
				String line;
				while ((line = reader.readLine()) != null) {

					writer.newLine();
					writer.write(line);
				}
				writer.close();
				reader.close();
			} catch (FileNotFoundException ex) {
				// TODO Auto-generated catch block
				System.out.println("The specified file doesn't exist.");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.getMessage();
				e.printStackTrace();
			}
		} else
			System.out.println("Cant do it");
	}

	void replaceCharsOfFile(File source, File dest, char oldChar, char newChar) {
		long start = System.currentTimeMillis();

		if (source == null) {
			System.out.println("No file found.");
		}

		else {
			reader = new BufferedReader(getFileReader(source));
			writer = new BufferedWriter(getFileWriter(dest));

			char ch;
			int temp = 0;
			try {

				while ((temp = reader.read()) != -1) {

					ch = (char) temp;
					if (ch == oldChar) {
						ch = newChar;
					}
					writer.append(ch);

				}
				reader.close();
				writer.close();
			} catch (IOException e) {

				e.printStackTrace();
			}

		}

		System.out.println(System.currentTimeMillis() - start);

	}

	FileWriter getFileWriter(File file) {
		try {
			return new FileWriter(file);
		} catch (IOException e) {

			e.printStackTrace();
		}
		return null;
	}

	FileReader getFileReader(File file) {
		try {
			return new FileReader(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("File Not found, enter a valid file name.");
		}
		return null;
	}

}
